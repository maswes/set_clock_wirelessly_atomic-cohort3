/*
 * angle_test.cpp
 *
 *  Created on: May 22, 2015
 *      Author: duaneellis
 */

#include "decode.h"

static void do_it( int a, int y, int x )
{
	int r;
	int err;

	r = get_degree_angle( x, y );

	err = r-a;
	if( err < 0 ){
		err = -err;
	}

	// we don't have a perfect converter
	if( err > 2 ){
		DONOT_USE_THIS.printf("\r\nFAIL @ angle: %d, y=%d, x=%d,r=%d\r\n",
		a,y,x,r );
	} else {
		DONOT_USE_THIS.printf(".");
		if( (a % 60) == 59 ){
			DONOT_USE_THIS.printf("\r\n");
		}
	}
}

void angle_test(void)
{

	// Scale:	50000
//	Deg		Y		X
do_it(	0	,	0	,	50000	);
do_it(	1	,	872	,	49992	);
do_it(	2	,	1744	,	49969	);
do_it(	3	,	2616	,	49931	);
do_it(	4	,	3487	,	49878	);
do_it(	5	,	4357	,	49809	);
do_it(	6	,	5226	,	49726	);
do_it(	7	,	6093	,	49627	);
do_it(	8	,	6958	,	49513	);
do_it(	9	,	7821	,	49384	);
do_it(	10	,	8682	,	49240	);
do_it(	11	,	9540	,	49081	);
do_it(	12	,	10395	,	48907	);
do_it(	13	,	11247	,	48718	);
do_it(	14	,	12096	,	48514	);
do_it(	15	,	12940	,	48296	);
do_it(	16	,	13781	,	48063	);
do_it(	17	,	14618	,	47815	);
do_it(	18	,	15450	,	47552	);
do_it(	19	,	16278	,	47275	);
do_it(	20	,	17101	,	46984	);
do_it(	21	,	17918	,	46679	);
do_it(	22	,	18730	,	46359	);
do_it(	23	,	19536	,	46025	);
do_it(	24	,	20336	,	45677	);
do_it(	25	,	21130	,	45315	);
do_it(	26	,	21918	,	44939	);
do_it(	27	,	22699	,	44550	);
do_it(	28	,	23473	,	44147	);
do_it(	29	,	24240	,	43730	);
do_it(	30	,	25000	,	43301	);
do_it(	31	,	25751	,	42858	);
do_it(	32	,	26495	,	42402	);
do_it(	33	,	27231	,	41933	);
do_it(	34	,	27959	,	41451	);
do_it(	35	,	28678	,	40957	);
do_it(	36	,	29389	,	40450	);
do_it(	37	,	30090	,	39931	);
do_it(	38	,	30783	,	39400	);
do_it(	39	,	31466	,	38857	);
do_it(	40	,	32139	,	38302	);
do_it(	41	,	32802	,	37735	);
do_it(	42	,	33456	,	37157	);
do_it(	43	,	34099	,	36567	);
do_it(	44	,	34732	,	35966	);
do_it(	45	,	35355	,	35355	);
do_it(	46	,	35966	,	34732	);
do_it(	47	,	36567	,	34099	);
do_it(	48	,	37157	,	33456	);
do_it(	49	,	37735	,	32802	);
do_it(	50	,	38302	,	32139	);
do_it(	51	,	38857	,	31466	);
do_it(	52	,	39400	,	30783	);
do_it(	53	,	39931	,	30090	);
do_it(	54	,	40450	,	29389	);
do_it(	55	,	40957	,	28678	);
do_it(	56	,	41451	,	27959	);
do_it(	57	,	41933	,	27231	);
do_it(	58	,	42402	,	26495	);
do_it(	59	,	42858	,	25751	);
do_it(	60	,	43301	,	25000	);
do_it(	61	,	43730	,	24240	);
do_it(	62	,	44147	,	23473	);
do_it(	63	,	44550	,	22699	);
do_it(	64	,	44939	,	21918	);
do_it(	65	,	45315	,	21130	);
do_it(	66	,	45677	,	20336	);
do_it(	67	,	46025	,	19536	);
do_it(	68	,	46359	,	18730	);
do_it(	69	,	46679	,	17918	);
do_it(	70	,	46984	,	17101	);
do_it(	71	,	47275	,	16278	);
do_it(	72	,	47552	,	15450	);
do_it(	73	,	47815	,	14618	);
do_it(	74	,	48063	,	13781	);
do_it(	75	,	48296	,	12940	);
do_it(	76	,	48514	,	12096	);
do_it(	77	,	48718	,	11247	);
do_it(	78	,	48907	,	10395	);
do_it(	79	,	49081	,	9540	);
do_it(	80	,	49240	,	8682	);
do_it(	81	,	49384	,	7821	);
do_it(	82	,	49513	,	6958	);
do_it(	83	,	49627	,	6093	);
do_it(	84	,	49726	,	5226	);
do_it(	85	,	49809	,	4357	);
do_it(	86	,	49878	,	3487	);
do_it(	87	,	49931	,	2616	);
do_it(	88	,	49969	,	1744	);
do_it(	89	,	49992	,	872	);
do_it(	90	,	50000	,	0	);
do_it(	91	,	49992	,	-873	);
do_it(	92	,	49969	,	-1745	);
do_it(	93	,	49931	,	-2617	);
do_it(	94	,	49878	,	-3488	);
do_it(	95	,	49809	,	-4358	);
do_it(	96	,	49726	,	-5227	);
do_it(	97	,	49627	,	-6094	);
do_it(	98	,	49513	,	-6959	);
do_it(	99	,	49384	,	-7822	);
do_it(	100	,	49240	,	-8683	);
do_it(	101	,	49081	,	-9541	);
do_it(	102	,	48907	,	-10396	);
do_it(	103	,	48718	,	-11248	);
do_it(	104	,	48514	,	-12097	);
do_it(	105	,	48296	,	-12941	);
do_it(	106	,	48063	,	-13782	);
do_it(	107	,	47815	,	-14619	);
do_it(	108	,	47552	,	-15451	);
do_it(	109	,	47275	,	-16279	);
do_it(	110	,	46984	,	-17102	);
do_it(	111	,	46679	,	-17919	);
do_it(	112	,	46359	,	-18731	);
do_it(	113	,	46025	,	-19537	);
do_it(	114	,	45677	,	-20337	);
do_it(	115	,	45315	,	-21131	);
do_it(	116	,	44939	,	-21919	);
do_it(	117	,	44550	,	-22700	);
do_it(	118	,	44147	,	-23474	);
do_it(	119	,	43730	,	-24241	);
do_it(	120	,	43301	,	-25000	);
do_it(	121	,	42858	,	-25752	);
do_it(	122	,	42402	,	-26496	);
do_it(	123	,	41933	,	-27232	);
do_it(	124	,	41451	,	-27960	);
do_it(	125	,	40957	,	-28679	);
do_it(	126	,	40450	,	-29390	);
do_it(	127	,	39931	,	-30091	);
do_it(	128	,	39400	,	-30784	);
do_it(	129	,	38857	,	-31467	);
do_it(	130	,	38302	,	-32140	);
do_it(	131	,	37735	,	-32803	);
do_it(	132	,	37157	,	-33457	);
do_it(	133	,	36567	,	-34100	);
do_it(	134	,	35966	,	-34733	);
do_it(	135	,	35355	,	-35356	);
do_it(	136	,	34732	,	-35967	);
do_it(	137	,	34099	,	-36568	);
do_it(	138	,	33456	,	-37158	);
do_it(	139	,	32802	,	-37736	);
do_it(	140	,	32139	,	-38303	);
do_it(	141	,	31466	,	-38858	);
do_it(	142	,	30783	,	-39401	);
do_it(	143	,	30090	,	-39932	);
do_it(	144	,	29389	,	-40451	);
do_it(	145	,	28678	,	-40958	);
do_it(	146	,	27959	,	-41452	);
do_it(	147	,	27231	,	-41934	);
do_it(	148	,	26495	,	-42403	);
do_it(	149	,	25751	,	-42859	);
do_it(	150	,	25000	,	-43302	);
do_it(	151	,	24240	,	-43731	);
do_it(	152	,	23473	,	-44148	);
do_it(	153	,	22699	,	-44551	);
do_it(	154	,	21918	,	-44940	);
do_it(	155	,	21130	,	-45316	);
do_it(	156	,	20336	,	-45678	);
do_it(	157	,	19536	,	-46026	);
do_it(	158	,	18730	,	-46360	);
do_it(	159	,	17918	,	-46680	);
do_it(	160	,	17101	,	-46985	);
do_it(	161	,	16278	,	-47276	);
do_it(	162	,	15450	,	-47553	);
do_it(	163	,	14618	,	-47816	);
do_it(	164	,	13781	,	-48064	);
do_it(	165	,	12940	,	-48297	);
do_it(	166	,	12096	,	-48515	);
do_it(	167	,	11247	,	-48719	);
do_it(	168	,	10395	,	-48908	);
do_it(	169	,	9540	,	-49082	);
do_it(	170	,	8682	,	-49241	);
do_it(	171	,	7821	,	-49385	);
do_it(	172	,	6958	,	-49514	);
do_it(	173	,	6093	,	-49628	);
do_it(	174	,	5226	,	-49727	);
do_it(	175	,	4357	,	-49810	);
do_it(	176	,	3487	,	-49879	);
do_it(	177	,	2616	,	-49932	);
do_it(	178	,	1744	,	-49970	);
do_it(	179	,	872	,	-49993	);
do_it(	180	,	0	,	-50000	);
do_it(	181	,	-873	,	-49993	);
do_it(	182	,	-1745	,	-49970	);
do_it(	183	,	-2617	,	-49932	);
do_it(	184	,	-3488	,	-49879	);
do_it(	185	,	-4358	,	-49810	);
do_it(	186	,	-5227	,	-49727	);
do_it(	187	,	-6094	,	-49628	);
do_it(	188	,	-6959	,	-49514	);
do_it(	189	,	-7822	,	-49385	);
do_it(	190	,	-8683	,	-49241	);
do_it(	191	,	-9541	,	-49082	);
do_it(	192	,	-10396	,	-48908	);
do_it(	193	,	-11248	,	-48719	);
do_it(	194	,	-12097	,	-48515	);
do_it(	195	,	-12941	,	-48297	);
do_it(	196	,	-13782	,	-48064	);
do_it(	197	,	-14619	,	-47816	);
do_it(	198	,	-15451	,	-47553	);
do_it(	199	,	-16279	,	-47276	);
do_it(	200	,	-17102	,	-46985	);
do_it(	201	,	-17919	,	-46680	);
do_it(	202	,	-18731	,	-46360	);
do_it(	203	,	-19537	,	-46026	);
do_it(	204	,	-20337	,	-45678	);
do_it(	205	,	-21131	,	-45316	);
do_it(	206	,	-21919	,	-44940	);
do_it(	207	,	-22700	,	-44551	);
do_it(	208	,	-23474	,	-44148	);
do_it(	209	,	-24241	,	-43731	);
do_it(	210	,	-25000	,	-43302	);
do_it(	211	,	-25752	,	-42859	);
do_it(	212	,	-26496	,	-42403	);
do_it(	213	,	-27232	,	-41934	);
do_it(	214	,	-27960	,	-41452	);
do_it(	215	,	-28679	,	-40958	);
do_it(	216	,	-29390	,	-40451	);
do_it(	217	,	-30091	,	-39932	);
do_it(	218	,	-30784	,	-39401	);
do_it(	219	,	-31467	,	-38858	);
do_it(	220	,	-32140	,	-38303	);
do_it(	221	,	-32803	,	-37736	);
do_it(	222	,	-33457	,	-37158	);
do_it(	223	,	-34100	,	-36568	);
do_it(	224	,	-34733	,	-35967	);
do_it(	225	,	-35356	,	-35356	);
do_it(	226	,	-35967	,	-34733	);
do_it(	227	,	-36568	,	-34100	);
do_it(	228	,	-37158	,	-33457	);
do_it(	229	,	-37736	,	-32803	);
do_it(	230	,	-38303	,	-32140	);
do_it(	231	,	-38858	,	-31467	);
do_it(	232	,	-39401	,	-30784	);
do_it(	233	,	-39932	,	-30091	);
do_it(	234	,	-40451	,	-29390	);
do_it(	235	,	-40958	,	-28679	);
do_it(	236	,	-41452	,	-27960	);
do_it(	237	,	-41934	,	-27232	);
do_it(	238	,	-42403	,	-26496	);
do_it(	239	,	-42859	,	-25752	);
do_it(	240	,	-43302	,	-25000	);
do_it(	241	,	-43731	,	-24241	);
do_it(	242	,	-44148	,	-23474	);
do_it(	243	,	-44551	,	-22700	);
do_it(	244	,	-44940	,	-21919	);
do_it(	245	,	-45316	,	-21131	);
do_it(	246	,	-45678	,	-20337	);
do_it(	247	,	-46026	,	-19537	);
do_it(	248	,	-46360	,	-18731	);
do_it(	249	,	-46680	,	-17919	);
do_it(	250	,	-46985	,	-17102	);
do_it(	251	,	-47276	,	-16279	);
do_it(	252	,	-47553	,	-15451	);
do_it(	253	,	-47816	,	-14619	);
do_it(	254	,	-48064	,	-13782	);
do_it(	255	,	-48297	,	-12941	);
do_it(	256	,	-48515	,	-12097	);
do_it(	257	,	-48719	,	-11248	);
do_it(	258	,	-48908	,	-10396	);
do_it(	259	,	-49082	,	-9541	);
do_it(	260	,	-49241	,	-8683	);
do_it(	261	,	-49385	,	-7822	);
do_it(	262	,	-49514	,	-6959	);
do_it(	263	,	-49628	,	-6094	);
do_it(	264	,	-49727	,	-5227	);
do_it(	265	,	-49810	,	-4358	);
do_it(	266	,	-49879	,	-3488	);
do_it(	267	,	-49932	,	-2617	);
do_it(	268	,	-49970	,	-1745	);
do_it(	269	,	-49993	,	-873	);
do_it(	270	,	-50000	,	-1	);
do_it(	271	,	-49993	,	872	);
do_it(	272	,	-49970	,	1744	);
do_it(	273	,	-49932	,	2616	);
do_it(	274	,	-49879	,	3487	);
do_it(	275	,	-49810	,	4357	);
do_it(	276	,	-49727	,	5226	);
do_it(	277	,	-49628	,	6093	);
do_it(	278	,	-49514	,	6958	);
do_it(	279	,	-49385	,	7821	);
do_it(	280	,	-49241	,	8682	);
do_it(	281	,	-49082	,	9540	);
do_it(	282	,	-48908	,	10395	);
do_it(	283	,	-48719	,	11247	);
do_it(	284	,	-48515	,	12096	);
do_it(	285	,	-48297	,	12940	);
do_it(	286	,	-48064	,	13781	);
do_it(	287	,	-47816	,	14618	);
do_it(	288	,	-47553	,	15450	);
do_it(	289	,	-47276	,	16278	);
do_it(	290	,	-46985	,	17101	);
do_it(	291	,	-46680	,	17918	);
do_it(	292	,	-46360	,	18730	);
do_it(	293	,	-46026	,	19536	);
do_it(	294	,	-45678	,	20336	);
do_it(	295	,	-45316	,	21130	);
do_it(	296	,	-44940	,	21918	);
do_it(	297	,	-44551	,	22699	);
do_it(	298	,	-44148	,	23473	);
do_it(	299	,	-43731	,	24240	);
do_it(	300	,	-43302	,	25000	);
do_it(	301	,	-42859	,	25751	);
do_it(	302	,	-42403	,	26495	);
do_it(	303	,	-41934	,	27231	);
do_it(	304	,	-41452	,	27959	);
do_it(	305	,	-40958	,	28678	);
do_it(	306	,	-40451	,	29389	);
do_it(	307	,	-39932	,	30090	);
do_it(	308	,	-39401	,	30783	);
do_it(	309	,	-38858	,	31466	);
do_it(	310	,	-38303	,	32139	);
do_it(	311	,	-37736	,	32802	);
do_it(	312	,	-37158	,	33456	);
do_it(	313	,	-36568	,	34099	);
do_it(	314	,	-35967	,	34732	);
do_it(	315	,	-35356	,	35355	);
do_it(	316	,	-34733	,	35966	);
do_it(	317	,	-34100	,	36567	);
do_it(	318	,	-33457	,	37157	);
do_it(	319	,	-32803	,	37735	);
do_it(	320	,	-32140	,	38302	);
do_it(	321	,	-31467	,	38857	);
do_it(	322	,	-30784	,	39400	);
do_it(	323	,	-30091	,	39931	);
do_it(	324	,	-29390	,	40450	);
do_it(	325	,	-28679	,	40957	);
do_it(	326	,	-27960	,	41451	);
do_it(	327	,	-27232	,	41933	);
do_it(	328	,	-26496	,	42402	);
do_it(	329	,	-25752	,	42858	);
do_it(	330	,	-25000	,	43301	);
do_it(	331	,	-24241	,	43730	);
do_it(	332	,	-23474	,	44147	);
do_it(	333	,	-22700	,	44550	);
do_it(	334	,	-21919	,	44939	);
do_it(	335	,	-21131	,	45315	);
do_it(	336	,	-20337	,	45677	);
do_it(	337	,	-19537	,	46025	);
do_it(	338	,	-18731	,	46359	);
do_it(	339	,	-17919	,	46679	);
do_it(	340	,	-17102	,	46984	);
do_it(	341	,	-16279	,	47275	);
do_it(	342	,	-15451	,	47552	);
do_it(	343	,	-14619	,	47815	);
do_it(	344	,	-13782	,	48063	);
do_it(	345	,	-12941	,	48296	);
do_it(	346	,	-12097	,	48514	);
do_it(	347	,	-11248	,	48718	);
do_it(	348	,	-10396	,	48907	);
do_it(	349	,	-9541	  ,	49081	);
do_it(	350	,	-8683	  ,	49240	);
do_it(	351	,	-7822	  ,	49384	);
do_it(	352	,	-6959	  ,	49513	);
do_it(	353	,	-6094	  ,	49627	);
do_it(	354	,	-5227	  ,	49726	);
do_it(	355	,	-4358	  ,	49809	);
do_it(	356	,	-3488	  ,	49878	);
do_it(	357	,	-2617	  ,	49931	);
do_it(	358	,	-1745	  ,	49969	);
do_it(	359	,	-873	  ,	49992	);
}
