/*
 * bg_serial.cpp
 *
 *  Created on: May 22, 2015
 *      Author: duaneellis
 */

#include "decode.h"
#include "stdarg.h"

Serial DONOT_USE_THIS(P0_0, P0_1);

class CQ { /* circular queue */
public:
	size_t wr, rd, len, nitems;
	uint8_t *buf;
	CQ( uint8_t *buf, size_t len );
	int get( void );
	int put( int c );
};

static uint8_t debug_circle_que_buf[ 256 ];

CQ dbg_q = CQ( debug_circle_que_buf,sizeof(debug_circle_que_buf));

CQ::CQ( uint8_t *_buf, size_t _len )
{
	nitems = 0;
	wr = 0;
	rd = 0;
	len = _len;
	buf = _buf;
}

static int __MY_disable_irq(void)
{
	uint32_t v;

	v = __get_PRIMASK();
	if( v & 1)
		return 1;
	else
		return 0;
}

int CQ::get(void)
{
	int r;
	int m;
	m = __MY_disable_irq();

	if( nitems == 0 ){
		r = EOF;
	} else {
		nitems--;
		r = buf[rd];
		rd = (rd+1) % len;
	}
	if( m ){
		__enable_irq();
	}
	return r;
}

int CQ::put(int d)
{
	int m;
	int r;

	m = __MY_disable_irq();

	if( nitems == len ){
		r = EOF;
	} else {
		nitems++;
		buf[ wr ] = d;
		wr = (wr+1) % len;
	}

	if( m ){
		__enable_irq();
	}
	return r;
}


static void serial_putc( int c )
{
	dbg_q.put(c);
}

static void serial_puts(const uint8_t *s)
{
	while(*s){
		serial_putc(*s);
		s++;
	}
}

void debug_vprintf( const char *fmt, va_list ap )
{
	static	char debug_printf_buf[100];

	vsnprintf( (char *)debug_printf_buf, sizeof(debug_printf_buf), fmt, ap );

	debug_printf_buf[sizeof(debug_printf_buf)-1] =0;
	serial_puts( (uint8_t *)debug_printf_buf );
}

void debug_printf( const char *fmt, ... )
{
	va_list ap;

	va_start(ap,fmt);
	debug_vprintf( fmt, ap );
	va_end(ap);
}

void debug_background_drain(int n)
{
	if( n < 0 ){
		n = 1000000;
	}
	for(;;){
		if( debug_background_hook() == EOF ){
			break;
		}
		n--;
		if( n < 0 ){
			return;
		}
	}
}

int debug_getc(void)
{
	if( DONOT_USE_THIS.readable() ){
		return DONOT_USE_THIS.getc();
	} else {
		return EOF;
	}
}

int debug_background_hook(void)
{
	int x;
	/*
	 ** Q: What is this?
	 **
	 ** Life would be simpler if I had
	 ** a full version of the Kiel tools
	 ** and could go past 32kbytes of code
	 ** Then I would invest the time to
	 ** make an IRQ based serial port.
	 **
	 ** A: What I really want is an interrupt
	 **    driven serial port, I don't have one
	 **
	 ** The soution is to print to a buffer
	 ** Then when we have nothing to do
	 ** an we can afford to hang printing a
	 ** *single* byte we call this function.
	 **
	 ** This function takes the text from the
	 ** circle queue - if there is no data
	 ** then we get EOF
	 **
	 ** If have data, then we print the byte
	 ** And that is when we might block for
	 ** that single byte
	 **
	 ** If we block we might not process our
	 ** little chunk of data in time and
	 ** our buffers will overflow :-(
	 */
	x = dbg_q.get();
	if( x != EOF ){
		/* because of the above you cannot
		 * use the "STREAM.printf()
		 *
		 * GRRR...
		 *
		 */
		if ( x == '\t' ){
			do {
				DONOT_USE_THIS.putc(' ');
				debug_col++;
#define TAB_WIDTH 4
			} while( (debug_col % TAB_WIDTH) != 0 )
				;
		} else if( x == '\n' ){
			debug_row++;
		} else if( x == '\r' ){
			debug_col = 0;
		} else {
			debug_col++;
		}

		DONOT_USE_THIS.putc(x);
	}
	if( x == EOF ){
		return EOF;
	} else {
		return 0;
	}
}

struct debug_rc {
	int r,c;
};
static int rc_stack_idx;
static struct debug_rc rc_stack[20];

void debug_rc_push(void)
{
	rc_stack[ rc_stack_idx ].r = debug_row;
	rc_stack[ rc_stack_idx ].c = debug_col;
	rc_stack_idx++;
}

void debug_rc_pop(void)
{
	rc_stack_idx--;
	debug_row = rc_stack[ rc_stack_idx ].r;
	debug_col = rc_stack[ rc_stack_idx ].c;
}


void debug_cursor_rc( int row, int col )
{
	debug_rc_push();
	debug_printf("\x1b[%d;%dH", row, col );
	debug_rc_pop();
}


void debug_cursor_cls(void)
{
	debug_rc_push();
	debug_printf("\x1b[H\x1b[J");
	debug_rc_pop();
}



void debug_rc_printf( int row, int col, const char *fmt, ... )
{
	va_list ap;
	va_start( ap,fmt );
	debug_rc_vprintf( row, col, fmt, ap );
	va_end(ap);
}

void debug_rc_vprintf( int row, int col, const char *fmt, va_list ap )
{
	debug_cursor_rc( row, col );
	debug_vprintf( fmt, ap );
}

void debug_cursor_bold( void )
{
	debug_rc_push();
	debug_printf("\x1b[1m");
	debug_rc_pop();
}

void debug_cursor_normal(void)
{
	debug_rc_push();
	debug_printf("\x1b[0m");
	debug_rc_pop();
}

