#define GLOBAL
#include "decode.h"

int dbg_halt = 0;
#if 1
DigitalOut AM_LED(P0_22);
#else
int AM_LED;
#endif

#if 0
DigitalOut PH_LED(LED2);
#else
int PH_LED;
#endif

#if 0
DigitalOut IRQ_LED(P0_22);
#else
int IRQ_LED;
#endif

#if 0
DigitalOut HB_LED(LED4);
#else
int HB_LED;
#endif




MODDMA dma;
MODDMA_LLI lliA;
MODDMA_LLI lliB;
MODDMA_Config dmacfg;

// #define ADC_DATA_REGISTER ((uint32_t)(&LPC_ADC->ADGDR))
#define ADC_DATA_REGISTER ((uint32_t)(&(LPC_ADC->ADDR5)))

// Function prototypes for IRQ callbacks.
// See definitions following main() below.
void TC0_callback(void);
void ERR0_callback(void);

#define ADC_CHNL_NUM  MODDMA::Channel_0

void setup_dma( void )
{
	MODDMA_Config *cfg;
	// Setup active DMA configuration
	// we start by setting it Buffer A
	cfg = &(dmacfg);
	cfg->channelNum( ADC_CHNL_NUM )
    		->srcMemAddr    ( ADC_DATA_REGISTER )
			->dstMemAddr    ( (uint32_t)(BUFA_START))
			->transferSize  ( BUFFER_LENGTH )
			->transferType  ( MODDMA::p2m )
			->transferWidth ( MODDMA::halfword ) //ARM : Word size is 32-bits
			->srcConn       ( MODDMA::ADC )
			->dstConn       ( 0 )
			// When done, we goto lliB
			->dmaLLI        ( (uint32_t) (&lliB))
			->attach_tc     ( &TC0_callback )
			->attach_err    ( &ERR0_callback )
			; // end conf.

	// Prepare configuration.
	dma.Setup( cfg );

	LPC_GPDMACH_TypeDef *pChannel = (LPC_GPDMACH_TypeDef *)dma.Channel_p( cfg->channelNum() );
	/* the DMA Setup Above - assumes
	 * the ADC is a 32bit register
	 * Yes it is, but the data is in the low
	 * 16bits and that is all we want
	 * so - we change the control register bits
	 */


	lliA.srcAddr( pChannel->DMACCSrcAddr );
	lliB.srcAddr( pChannel->DMACCSrcAddr );

	lliA.dstAddr( (uint32_t)(BUFA_START) );
	lliB.dstAddr( (uint32_t)(BUFB_START) );

	lliA.nextLLI( (uint32_t)(&lliB) );
	lliB.nextLLI( (uint32_t)(&lliA) );

	lliA.control( pChannel->DMACCControl + BUFFER_LENGTH );
	lliB.control( pChannel->DMACCControl + BUFFER_LENGTH );
}

void setup_clocks(void)
{
	uint32_t v;

	/* ADC clock needs to be 1/4 of the PCLK
	 * 93.6mhz / 8 = 11.7mhz
	 * Given 65 ADC clocks = 180khz
	 * which is exactly 1.5 times our signal rate
	 */

	v = LPC_SC->PCLKSEL0;
	v &= ~(0x3 << 24);
	v |= (0x3 << 24);
	LPC_SC->PCLKSEL0 = v;

}

void setup_adc(void)
{
	uint32_t v;
	LPC_PINCON->PINSEL3 &= ~(3UL << 30);  /* P1.31, Mbed p20. */
	LPC_PINCON->PINSEL3 |=  (3UL << 30);


	/* Power up the ADC */
	LPC_SC->PCONP    |=  (1UL << 12);

	/* The PCLOCK = 93.6mhz
	 * The input to the ADC must be less then 13mhz
	 * So we divide by 8 - giving 11.7mhz
	 */

	/* build the ADC Control Register */
	v = 0;
	/* select channel 5 bits[7:0]*/
	v |= (1 << 5);
	/* Clock Divisor, bits[15:8] */
	/*  GIVEN: 93.6mhz peripheral clock
	 * DIVIDE:  8
	 * RESULT: 11.7mhz ADC clock
	 * ADC requires 65 clocks per conversion
	 * Which gives us a 180khz sample rate
	 */
	int adc_clock_div = 1;
	v |= ((adc_clock_div - 1 ) << 8 );
	/* Burst Mode bit[16]*/
	v |= (1 << 16);
	/* reserved zero, bits[20:17] */
	v |= 0;

	/* Enable the ADC bit[21] */
	v |= (1UL << 21);
	/* bits[23:22] = reserved, zero */
	v |= 0;
	/* Bits[26:24] = No start setting */
	v |= 0;
	/* MUST BE zero = See data sheet and below */

	/* bits[27] = not applicable */
	v |= 0;
	/* bits[31:28] = reserved zero */
	v |= 0;
	LPC_ADC->ADCR =  v;

	/* interrupt enable: ADGINTEN = 0 */
	/* From the data sheet:
	 * Remark: START bits must be 000 when BURST = 1
	 * or conversions will not start. If BURST is set to 1,
	 * the ADGINTEN bit in the AD0INTEN register
	 * (Table 534) must be set to 0.
	 */
	LPC_ADC->ADINTEN = 0x100;
}


// Configuration callback on TC
void TC0_callback(void)
{
	int x;
	IRQ_LED = 1;
	x = totalComplete;
	x = x + 1;
	totalComplete = x;
	//BUFFER_LED = x & 1;

	process_samples(active_buffer);

	//decode_this_sample();

	/* now switch the buffer letter */
	// we start with "A" = 0x41
	// Binary:  0100 0001
	//    XOR:  0000 0011
	// ===================
	//          0100 0010 = 0x62 (B)
	//                 11
	//          = 65 = our A
	// Do it again, and we get B
	active_buffer = active_buffer ^ 3;
	IRQ_LED = 0;

	decode_sample( THIS_am_signal, THIS_pm_signal );

	dbg_hist[ dbg_hist_index ].bin_angle = THIS_pm_signal;
	dbg_hist[ dbg_hist_index ].mag8bit   = THIS_am_signal;
	dbg_hist_index = (dbg_hist_index+1) % DBG_HIST_SIZE;
	if( dbg_hist_index == 0 ){
	//	dbg_halt = 1;
	}
}

// Configuration callback on Error
void ERR0_callback(void)
{
	// Switch off burst conversions.
	LPC_ADC->ADCR |= ~(1UL << 16);
	LPC_ADC->ADINTEN = 0;
	dma.clearErrIrq();
	error("Oh no! My Mbed EXPLODED! :( Only kidding, go find the problem");
}


void dump_decode_data(void)
{
	int x;
	for( x = 0 ; x < 60 ; x++ ){
		debug_printf("%d", sixty_symbols[x].am_data);
	}
	debug_printf("\r\n");
	for( x = 0 ; x < 60 ; x++ ){
		debug_printf("%d", sixty_symbols[x].pm_data);
	}
	debug_printf("\r\n");
	debug_printf("\r\n");

}

void real_decoding(void)
{
	int allflag;
	// Enable configuration.
	active_buffer = 'A';
	dma.Enable( &(dmacfg) );

	int last_status = -1;
	int this_status = 1234;

	allflag = 1;

	while (1) {

		/* we need to service the serial port buffer */
		debug_background_hook();
		if( dbg_halt ){
			/* For debug reasons we might want to halt
			 * and print our debug buffers
			 */
			break;
		}

		/* print how many times we have captured data */
		this_status = totalComplete / 100;
		if( this_status != last_status ){
			last_status = this_status;
			debug_cursor_rc( 3,1 );
			debug_printf("%5d sec", this_status);
		}

		if( dbg_decode_dump_flag ){
			dbg_decode_dump_flag = 0;
			if( debug_getc() != EOF ){
				allflag = 1;
			}

			dbg_decode_dump(allflag);
			allflag = 0;
		}
	}
}


int main()
{
	int x;

	// Setup the serial port to print out results.
	// this is the only time we use this
	DONOT_USE_THIS.baud(115200);
	//angle_test();

	debug_printf("ADC with DMA example\r\n");
	debug_printf("===================\r\n");

	// We use the ADC irq to trigger DMA and the manual says
	// that in this case the NVIC for ADC must be disabled.
	NVIC_DisableIRQ(ADC_IRQn);

	setup_clocks();

	setup_adc();

	setup_dma();

#if 1
	real_decoding();
#else
	test_decoding();
#endif

	dma.Disable( ADC_CHNL_NUM ) ;
	LPC_ADC->ADCR |= ~(1UL << 16);
	LPC_ADC->ADINTEN = 0;


	for( x = 0 ; x < DBG_HIST_SIZE ; x++ ){
		DONOT_USE_THIS.printf("%5d, %4d, %4d\r\n",
				x,
				dbg_hist[x].mag8bit,
				dbg_hist[x].bin_angle);
	}
	debug_printf("Bye Bye\n");
}

