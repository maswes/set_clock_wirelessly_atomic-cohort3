/*
 * decode.h
 *
 *  Created on: May 22, 2015
 *      Author: duaneellis
 */

#if !defined(DECODE_H)
#define DECODE_H

#include "mbed.h"
#include "MODDMA.h"
#include "stdint.h"
#include "string.h"
#include "stdarg.h"
#include "time.h"

/* Goal 180khz adc sample rate.
 *
 * And a 10 millisecond packet of data rate
 */

#if !defined(GLOBAL)
#define GLOBAL extern
#endif

GLOBAL struct tm AM_time;
GLOBAL int       AM_time_valid;
GLOBAL struct tm PM_time;
GLOBAL int       PM_time_valid;
GLOBAL int       PM_was_inverted;


#define MSECS_PER_SAMPLE   10
#define SAMPLES_PE_SECOND  (1000 / MSECS_PER_SAMPLE)
#define SAMPLES_nSECS(N)   ((N)*MSECS_PER_SAMPLE)



/* how many samples are complete */
GLOBAL int totalComplete;

/* Given our sample rate of 180khz
 * We need 10 milliseconds of data
 *  Thus  180,000 * 0.01 = 1800
 * KEY POINT the buffer must be divisible by
 * the length of our local oscillator wave form
 * The local oscillator wave form is 3 samples
 * Thus, it works 1800 / 3 = 600
 *
 */
#define BUFFER_LENGTH  1800 // length is in samples
/* a power of 2 number that is less then
** the buffer length
*/
#define BUFFER_LENGTH_LOWER_LOG2 (1024)


/* We filter/average our signal over N samples
** This average is used to calculate our DC bias
** and is used to create an "ac-coupled" interface
*/
#define AVG_LENGTH 16

#define DATABUF_LEN ((BUFFER_LENGTH * 2) + AVG_LENGTH)

struct _10msec_sample {
	uint8_t mag8bit;
	/* this is 0 to 255 */
	/* it is not 0..360 */
	uint8_t bin_angle;
};


struct nist_symbol {
	uint8_t am_data;
	uint8_t pm_data;
};

GLOBAL	int active_buffer;
GLOBAL	uint32_t dma_buffer[ DATABUF_LEN ];
GLOBAL	int old_avg_sum;
GLOBAL	int max_mag_seen;
GLOBAL	int min_mag_seen;

GLOBAL  int THIS_am_signal;
GLOBAL  int THIS_pm_signal;
GLOBAL  int STUCK_counter;
GLOBAL  int CUR_sample_number;
GLOBAL  int AM_h_counter;
GLOBAL  int AM_l_counter;
GLOBAL  int AM_symbol;
GLOBAL  int LOUD_PM_symbol;
GLOBAL  int AM_symbol_last;
GLOBAL  int PM_symbol;
GLOBAL  int PM_last;
GLOBAL  int PM_change_time;


GLOBAL struct nist_symbol sixty_symbols[ 60 ];

#define BUF_RAW_START  (&dma_buffer[0])
#define BUFA_START     (&dma_buffer[AVG_LENGTH])
#define BUFB_START     (&dma_buffer[AVG_LENGTH + BUFFER_LENGTH ])
#define BUFB_AVG_END   (&dma_buffer[AVG_LENGTH + BUFFER_LENGTH + BUFFER_LENGTH - AVG_LENGTH])
#define AVG_LENGTH_BYTES  (AVG_LENGTH * sizeof(dma_buffer[0]))

void reset_decoder(void);

void angle_test(void);
int get_degree_angle( int x, int y);

void process_samples(int buf_id);

GLOBAL int debug_background_hook(void);
// See bg_serial for details about this
extern Serial DONOT_USE_THIS;
void debug_printf( const char *fmt, ... );
void debug_vprintf( const char *fmt, va_list ap );
int debug_next_tx( void );

#define DBG_HIST_SIZE 4000
GLOBAL struct _10msec_sample dbg_hist[ DBG_HIST_SIZE ];
GLOBAL int dbg_hist_index;

void   decode_sample(int am_data, int ph_data );
void test_decoding(void);
void dump_decode_data(void);

void dbg_decode_dump(int doitall);
GLOBAL int dbg_decode_dump_flag;


int debug_getc(void);

void debug_background_drain(int n);
void debug_cursor_rc( int row, int col );
void debug_cursor_cls(void);
void debug_rc_printf( int row, int col, const char *fmt, ... );
void debug_rc_vprintf( int row, int col, const char *fmt, va_list ap );
void debug_cursor_bold( void );
void debug_cursor_normal(void);
GLOBAL int debug_row;
GLOBAL int debug_col;

#endif
