################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../MODDMA/DATALUTS.cpp \
../MODDMA/INIT.cpp \
../MODDMA/MODDMA.cpp \
../MODDMA/SETUP.cpp 

C_SRCS += \
../MODDMA/ChangeLog.c 

OBJS += \
./MODDMA/ChangeLog.o \
./MODDMA/DATALUTS.o \
./MODDMA/INIT.o \
./MODDMA/MODDMA.o \
./MODDMA/SETUP.o 

C_DEPS += \
./MODDMA/ChangeLog.d 

CPP_DEPS += \
./MODDMA/DATALUTS.d \
./MODDMA/INIT.d \
./MODDMA/MODDMA.d \
./MODDMA/SETUP.d 


# Each subdirectory must supply rules for building sources it contributes
MODDMA/%.o: ../MODDMA/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -D__CODE_RED -DCPP_USE_HEAP -DTARGET_LPC1768 -DTARGET_M3 -DTARGET_CORTEX_M -DTARGET_NXP -DTARGET_LPC176X -DTARGET_MBED_LPC1768 -DTOOLCHAIN_GCC_CR -DTOOLCHAIN_GCC -D__CORTEX_M3 -DARM_MATH_CM3 -DMBED_BUILD_TIMESTAMP=1432352557.99 -D__MBED__=1 -I"C:\capstone\out2\rx_lpc2" -I"C:\capstone\out2\rx_lpc2\MODDMA" -I"C:\capstone\out2\rx_lpc2\mbed" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TOOLCHAIN_GCC_CR" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TARGET_NXP" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TARGET_NXP\TARGET_LPC176X" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TARGET_NXP\TARGET_LPC176X\TARGET_MBED_LPC1768" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -mcpu=cortex-m3 -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

MODDMA/%.o: ../MODDMA/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -D__CODE_RED -DCPP_USE_HEAP -DTARGET_LPC1768 -DTARGET_M3 -DTARGET_CORTEX_M -DTARGET_NXP -DTARGET_LPC176X -DTARGET_MBED_LPC1768 -DTOOLCHAIN_GCC_CR -DTOOLCHAIN_GCC -D__CORTEX_M3 -DARM_MATH_CM3 -DMBED_BUILD_TIMESTAMP=1432352557.99 -D__MBED__=1 -I"C:\capstone\out2\rx_lpc2" -I"C:\capstone\out2\rx_lpc2\MODDMA" -I"C:\capstone\out2\rx_lpc2\mbed" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TOOLCHAIN_GCC_CR" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TARGET_NXP" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TARGET_NXP\TARGET_LPC176X" -I"C:\capstone\out2\rx_lpc2\mbed\TARGET_LPC1768\TARGET_NXP\TARGET_LPC176X\TARGET_MBED_LPC1768" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -mcpu=cortex-m3 -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


