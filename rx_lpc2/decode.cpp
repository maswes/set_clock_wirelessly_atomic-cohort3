#include "decode.h"

// the LPC mktime() function is broken
// I took this from http://www.opensource.apple.com/source/ntp/ntp-13/ntp/libntp/mktime.c


static int	mon_lengths[2][12] = {
	{ 31, 59, 90, 120, 151,181,212,243,273,304,334,365 },
	{ 31, 60,91,121,152,182,213,244,274,305,335,366 }
};


#define is_leapyear( Y )  (((Y)%4)==0)

void reset_decoder(void)
{
	STUCK_counter = 0;
	AM_h_counter = 0;
	AM_l_counter = 0;
	max_mag_seen = -(1<<20);
	min_mag_seen = +(1<<20);
	memset( (void *)(&sixty_symbols[0]), 0, sizeof(sixty_symbols) );
}


static int bcd_am( int b8, int b4, int b2, int b1 )
{
	if( b8 > 0 )
		b8 = sixty_symbols[ b8 ].am_data * 8;
	else
		b8 = 0;

	if( b4 > 0 )
		b4 = sixty_symbols[ b4 ].am_data * 4;
	else
		b4 = 0;

	if( b2 > 0 )
		b2 = sixty_symbols[ b2 ].am_data * 2;
	else
		b2 = 0;

	if( b1 > 0 )
		b1 = sixty_symbols[ b1 ].am_data * 1;
	else
		b1 = 0;

	return b8+b4+b2+b1;
}


static void am_time_decode(void)
{
	memset( (void *)(&AM_time), 0, sizeof(AM_time) );
	AM_time.tm_min = (bcd_am( -1, 1,2,3 ) * 10) + bcd_am( 5,6,7,8 );
	AM_time.tm_hour =(bcd_am( -1,-1,12,13)*10) + bcd_am( 15,16,17,18);
	AM_time.tm_yday =(bcd_am( -1,-1,22,23)*100) + (bcd_am(25,26,27,28)*10)+bcd_am(30,31,32,33);
	AM_time.tm_year =(bcd_am( 45,46,47,48)*10) + bcd_am(50,51,52,53) + (2000-1900);

	/* convert year day into month and month day */

	/* Normalize */

	for( AM_time.tm_mon = 0 ;AM_time.tm_mon < 12 ; AM_time.tm_mon++ ){

		if(AM_time.tm_yday < mon_lengths[ is_leapyear(AM_time.tm_year ) ][ AM_time.tm_mon ] ){
			break;
		}
	}

	if( AM_time.tm_mon ){
		AM_time.tm_yday -= mon_lengths[ is_leapyear(AM_time.tm_year ) ][ AM_time.tm_mon-1 ];
	}

	AM_time.tm_mday = AM_time.tm_yday;

	// A real implementation would have all the fields correct
	// the LPC mktime() function is broken so we don't use that
	// we do this our self
	// and we have enough fields for our purpose complete

	AM_time_valid = 1;
}



static const int sync_T[13] = {
		0,0,
		1,1,1,
		0,
		1,1,
		0,
		1,
		0,0,0
};

static int pm_decodenum;

static  time_t PM_BIT( time_t v, int bitnum, int weight )
{
	int bv;
	bv = sixty_symbols[bitnum].pm_data;
	bv = bv << weight;
	v += bv;
	return v;
}


static void pm_time_decode(void)
{
	int x;
	int y;

	pm_decodenum = (pm_decodenum + 1) & 0x0f;
	y = 0;
	for( x = 0 ; x < 13 ; x++ ){
		if( sync_T[x] == sixty_symbols[x].pm_data ){
			y++;
		} else {
			y--;
		}
	}
	if( y == -13 ){
		PM_was_inverted = !PM_was_inverted;
		/* data is inverted */
		PM_symbol = !PM_symbol;

		/* flip the decoded data */
		for( x = 0 ; x < 60 ; x++ ){
			sixty_symbols[x].pm_data = !(sixty_symbols[x].pm_data);
		}
		y = -y;
	}

	debug_cursor_rc(18,1);
	debug_printf("cor: % 2d", y );
	if( y != 13 ){
		PM_time_valid = 0;
	}

	memset( (void *)(&PM_time), 0, sizeof(PM_time) );

	time_t  m;

	m = 0;
	m = PM_BIT( m, 18,25 );
	m = PM_BIT( m, 20,24 );
	m = PM_BIT( m, 21,23 );
	m = PM_BIT( m, 22,22 );
	m = PM_BIT( m, 23,21 );
	m = PM_BIT( m, 24,20 );
	m = PM_BIT( m, 25,19 );
	m = PM_BIT( m, 26,18 );
	m = PM_BIT( m, 27,17 );
	m = PM_BIT( m, 28,16 );
	/* we skip the R bit */
	m = PM_BIT( m, 30,15 );
	m = PM_BIT( m, 31,14 );
	m = PM_BIT( m, 32,13 );
	m = PM_BIT( m, 33,12 );
	m = PM_BIT( m, 34,11 );
	m = PM_BIT( m, 35,10 );
	m = PM_BIT( m, 36, 9 );
	m = PM_BIT( m, 37, 8 );
	m = PM_BIT( m, 38, 7 );
	/* we skip the R bit */
	m = PM_BIT( m, 40, 6 );
	m = PM_BIT( m, 41, 5 );
	m = PM_BIT( m, 42, 4 );
	m = PM_BIT( m, 43, 3 );
	m = PM_BIT( m, 44, 2 );
	m = PM_BIT( m, 45, 1 );
	m = PM_BIT( m, 46, 0 );

	/* above is minutes since 1-jan-2000 */
	/* convert to seconds */
	m = m * 60;

	/* now add "unixtime" for 1-jan-2000*/
	m = m + 0x386db400;

	PM_time = *localtime( &m );

	debug_printf(" pm time: 0x%08x ", x );
	/* normalize */


	PM_time_valid = 1;
}


static void time_decode(void)
{
	am_time_decode();
	pm_time_decode();
}

static void ph_decode(int pm_data)
{
	int angle;
	if( (pm_data < 64) && (PM_last > 192) ){
		angle = pm_data + 256 - PM_last;
	} else if( (pm_data > 192) && (PM_last < 64) ){
		angle = pm_data - (256 - PM_last);
	} else {
		angle = pm_data - PM_last;
	}
	PM_last = pm_data;

	if( angle < 0 ){
		angle = -angle;
	}
	if( angle > 64 ){
		PM_change_time = CUR_sample_number;
		PM_symbol = !PM_symbol;
//		printf("@ %d - Phase = %d\n", PH_change_time, PH_symbol);
	}
}


static void am_decode( int am_data )
{
#define TWO_THIRD  ((2*255)/3)
#define ONE_THIRD  ((1*255)/3)

	int am_this;
	if( AM_symbol_last == 'L' ){
		if( am_data > TWO_THIRD ){
			am_this = 'H';
		} else {
			am_this = 'L';
		}
	} else {
		if( am_data < ONE_THIRD ){
			am_this = 'L';
		} else {
			am_this = 'H';
		}
	}

	if( am_this == 'H' ){
		LOUD_PM_symbol = PM_symbol;
		AM_h_counter++;
	} else {
		AM_l_counter++;
	}

	if( am_this == AM_symbol_last ){
		STUCK_counter++;
		if( STUCK_counter > 200 ){
			reset_decoder();
		}
		return;
	}

	STUCK_counter = 0;

	if( !((AM_symbol_last == 'H') && (am_this == 'L')) ){
		/* rising edge */
		AM_symbol_last = am_this;
		return;
	}

	/* else: Falling edge */
	AM_symbol_last = am_this;

	int total_time;

	/* in samples */
	total_time = AM_h_counter + AM_l_counter;
	/* in milliseconds */
	total_time *= 100;

	int x;
	/* convert the H into milliseconds high */
	x = AM_h_counter * 100 * 1000;
	x = x / total_time;

	/* milliseconds hi or percent lo */
	//  Symbols are  800/200 msecs
	//  or           500/500
	//  or           200/800
	//
	// Or +/-300 from 500msecs
	// Half of 300 = 150
	//
	// SO 500 + 150 = 650
	//    500 - 150 = 350
	//
	if( x > 650 ){
		/* we have been high for more then 650 milliseconds */
		AM_symbol = 0;
	} else if( x < 350 ){
		/* marker */
		AM_symbol = 2;
	} else {
		/* and the 1 value */
		AM_symbol = 1;
	}
	AM_h_counter = 0;
	AM_l_counter = 0;

	/*
	 *  Move all symbols over by 1
	 */

	memmove( (void *)(&sixty_symbols[0]),
			 (void *)(&sixty_symbols[1]),
			 sizeof(sixty_symbols) - sizeof(sixty_symbols[0]) );

	/* insert the new symbol at the end */
	sixty_symbols[ 59 ].am_data = AM_symbol;
	sixty_symbols[ 59 ].pm_data = PM_symbol;
	dbg_decode_dump_flag = 1;

	int n;
	n = 0;
	for( x = 0 ; x < 59 ; x++ ){
		if( (sixty_symbols[x].am_data==2) && (sixty_symbols[x+1].am_data==2) ){
			n++;
		}
	}
	if( n > 1 ){
		reset_decoder();
		return;
	}
	if( (sixty_symbols[0].am_data == 2) && (sixty_symbols[59].am_data==2) ){
		time_decode();
	}
}


void decode_sample(  int am_data,  int ph_data )
{
	ph_decode( ph_data );
	am_decode( am_data );
}


static const char *months[12] = {
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

void dbg_decode_dump(int doitall)
{
	int x;
	int y;

	if( doitall ){
		debug_cursor_cls();
	}

	int r;

	r = 5;

	if( doitall ){
		debug_cursor_rc( r,1 );
		debug_printf("BitNum: ");
		debug_cursor_rc(r,10);
		for( x = 0 ; x < 60 ; x++ ){
			if( (x%10) == 0 ){
				debug_printf("%d", x/10);
			} else {
				debug_printf(" ");
			}
			if( (x%10) == 9 ){
				debug_printf(" ");
			}
		}
	}

	r++;
	if( doitall ){
		debug_cursor_rc( r,1 );
		debug_printf("AM Sig: ");
	}
	debug_cursor_rc( r, 10 );
	for( x = 0 ; x < 60 ; x++ ){
		y = sixty_symbols[x].am_data;
		if( y == 2 ){
			debug_cursor_bold();
		}
		debug_printf("%d", y );
		if( y == 2 ){
			debug_cursor_normal();
		}
		if( (x%10) == 9 ){
			debug_printf(" ");
		}
	}

	r++;

	if( doitall ){
		debug_cursor_rc(r,1);
		debug_printf("PH Sig: ");
	}
	debug_cursor_rc( r,10);
	for( x = 0 ; x < 60 ; x++ ){
		debug_printf("%d", sixty_symbols[x].pm_data );
		if( (x%10) == 9 ){
			debug_printf(" ");
		}
	}

	r += 2;

	debug_cursor_rc( r, 1 );
	debug_printf("AM: (%3s) %s %2d %4d  %02d:%02d:%02d",
			(AM_time_valid ? "yes" : "no"),
			months[ AM_time.tm_mon ],
			AM_time.tm_mday,
			AM_time.tm_year + 1900,
			AM_time.tm_hour,
			AM_time.tm_min,
			AM_time.tm_sec);


	r += 2;
	debug_cursor_rc( r, 1 );
	debug_printf("PM: (%3s) %s %2d %4d  %02d:%02d:%02d",
			(PM_time_valid ? "yes" : "no"),
			months[ PM_time.tm_mon ],
			PM_time.tm_mday,
			PM_time.tm_year + 1900,
			PM_time.tm_hour,
			PM_time.tm_min,
			PM_time.tm_sec );
}
