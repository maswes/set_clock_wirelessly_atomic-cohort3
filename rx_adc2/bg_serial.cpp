#include "decode.h"
#include "stdarg.h"

Serial DONOT_USE_THIS(USBTX, USBRX);

class CQ { /* circular queue */
	public:
		size_t wr, rd, len, nitems;
		uint8_t *buf;
		CQ( uint8_t *buf, size_t len );
		int get( void );
		int put( int c );
};

static uint8_t debug_circle_que_buf[ 256 ];
	
CQ dbg_q = CQ( debug_circle_que_buf,sizeof(debug_circle_que_buf));

CQ::CQ( uint8_t *_buf, size_t _len )
{
	nitems = 0;
	wr = 0;
	rd = 0;
	len = _len;
	buf = _buf;
}

int CQ::get(void)
{
	int r;
	int m;
	m = __disable_irq();
	
	if( nitems == 0 ){
		r = EOF;
	} else {
		nitems--;
		r = buf[rd];
		rd = (rd+1) % len;
	}
	if( m ){
		__enable_irq();
	}
	return r;
}

int CQ::put(int d)
{
	int m;
	int r;
	
	m = __disable_irq();

	if( nitems == len ){
		r = EOF;
	} else {
		nitems++;
		buf[ wr ] = d;
		wr = (wr+1) % len;
	}
	
	if( m ){
		__enable_irq();
	}
	return r;
}
	

static void serial_putc( int c )
{
	dbg_q.put(c);
}

static void serial_puts(const uint8_t *s)
{
	while(*s){
		serial_putc(*s);
		s++;
	}
}

void debug_printf( const char *fmt, ... )
{
static	char debug_printf_buf[100];
	
	va_list ap;
	
	va_start(ap,fmt);
	vsnprintf( (char *)debug_printf_buf, sizeof(debug_printf_buf), fmt, ap );
	va_end(ap);
	debug_printf_buf[sizeof(debug_printf_buf)-1] =0;
	serial_puts( (uint8_t *)debug_printf_buf );
}

void debug_background_hook(void)
{
	int x;
	/*
	** Q: What is this?
  **
	** Life would be simpler if I had
	** a full version of the Kiel tools
	** and could go past 32kbytes of code
	** Then I would invest the time to
	** make an IRQ based serial port.
  **
	** A: What I really want is an interrupt
	**    driven serial port, I don't have one
	**   
	** The soution is to print to a buffer
	** Then when we have nothing to do 
	** an we can afford to hang printing a
	** *single* byte we call this function.
	**
	** This function takes the text from the
	** circle queue - if there is no data
	** then we get EOF
	**
	** If have data, then we print the byte
	** And that is when we might block for
	** that single byte
	**
	** If we block we might not process our
	** little chunk of data in time and
	** our buffers will overflow :-(
	*/
	x = dbg_q.get();
	if( x != EOF ){
		/* because of the above you cannot
		 * use the "STREAM.printf()
		 * 
		 * GRRR...
		 *
		 */
		DONOT_USE_THIS.putc(x);
	}
}
