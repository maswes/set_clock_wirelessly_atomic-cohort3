#include "mbed.h"
#include "MODDMA.h"
#include "stdint.h"
#include "string.h"

/* Goal 180khz adc sample rate.
 *   
 * And a 10 millisecond packet of data rate
 */
 
#if !defined(GLOBAL)
#define GLOBAL extern
#endif

#define SAMPLES_nSECS(N)   ((N)*100) 

/* DEBUG LEDS */
#define LED_MAG   led1
#define LED_PHASE led2
extern DigitalOut led1;
extern DigitalOut led2;
extern DigitalOut led3;
extern DigitalOut led4;

/* how many samples are complete */
GLOBAL int totalComplete;

/* Given our sample rate of 180khz
 * We need 10 milliseconds of data
 *  Thus  180,000 * 0.01 = 1800
 * KEY POINT the buffer must be divisible by
 * the length of our local oscilator wave form
 * The local oscillator wave form is 3 samples 
 * Thus, it works 1800 / 3 = 600
 * 
 */
#define BUFFER_LENGTH  1800 // length is in samples
/* a power of 2 number that is less then 
** the buffer length 
*/
#define BUFFER_LENGTH_LOWER_LOG2 (1024)


/* We filter/avergae our signal over N samples
** This average is used to calculate our DC bais
** and is used to create an "ac-coupled" interface
*/
#define AVG_LENGTH 16

#define DATABUF_LEN ((BUFFER_LENGTH * 2) + AVG_LENGTH)

struct _10msec_sample {
	uint8_t mag8bit;
	/* this is 0 to 255 */
	/* it is not 0..360 */
	uint8_t bin_angle;
};

struct nist_symbol {
	uint8_t am_data;
	uint8_t ph_data;
};

GLOBAL	int active_buffer;
GLOBAL	uint32_t dma_buffer[ DATABUF_LEN ];
GLOBAL	int old_avg_sum;
GLOBAL	int max_mag_seen;
GLOBAL	int min_mag_seen;
GLOBAL	int am_h_counter;
GLOBAL	int am_l_counter;
GLOBAL	int state_counter;
GLOBAL	int decode_state;
GLOBAL	int last_am_level;

GLOBAL struct _10msec_sample  this_sample;
GLOBAL uint8_t                prev_am_level;
GLOBAL uint8_t                this_am_level;
GLOBAL uint8_t                prev_ph_angle;
GLOBAL uint8_t                this_ph_angle;
GLOBAL struct nist_symbol sixty_symbols[ 60 ];
	
#define BUF_RAW_START  (&dma_buffer[0])
#define BUFA_START     (&dma_buffer[AVG_LENGTH])
#define BUFB_START     (&dma_buffer[AVG_LENGTH + BUFFER_LENGTH ])
#define BUFB_AVG_END   (&dma_buffer[AVG_LENGTH + BUFFER_LENGTH + BUFFER_LENGTH - AVG_LENGTH])
#define AVG_LENGTH_BYTES  (AVG_LENGTH * sizeof(dma_buffer[0]))

void angle_test(void);
int get_degree_angle( int x, int y);

void process_samples(int buf_id);

GLOBAL void debug_background_hook(void);
// See bg_serial for details about this
extern Serial DONOT_USE_THIS;
void debug_printf( const char *fmt, ... );
int debug_next_tx( void );

#define DBG_HIST_SIZE 2000
GLOBAL struct _10msec_sample dbg_hist[ DBG_HIST_SIZE ];
GLOBAL int dbg_hist_index;

