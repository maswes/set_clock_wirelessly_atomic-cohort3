#define GLOBAL
#include "decode.h"

#define DMA_DEBUG   1
#define CLOCK_DEBUG 1

int dbg_halt = 0;
DigitalOut AM_LED(LED1);
DigitalOut PH_LED(LED2);
DigitalOut IRQ_LED(LED3);
DigitalOut HB_LED(LED4);



MODDMA dma;
MODDMA_LLI lliA;
MODDMA_LLI lliB;
MODDMA_Config dmacfg;

// #define ADC_DATA_REGISTER ((uint32_t)(&LPC_ADC->ADGDR))
#define ADC_DATA_REGISTER ((uint32_t)(&(LPC_ADC->ADDR5)))

// Function prototypes for IRQ callbacks.
// See definitions following main() below.
void TC0_callback(void);
void ERR0_callback(void);

#define ADC_CHNL_NUM  MODDMA::Channel_0

void setup_dma( void )
{
    MODDMA_Config *cfg;
// Setup active DMA configiuration
// we start by setting it Buffer A
    cfg = &(dmacfg);
    cfg->channelNum( ADC_CHNL_NUM )
    ->srcMemAddr    ( ADC_DATA_REGISTER )
    ->dstMemAddr    ( (uint32_t)(BUFA_START))
    ->transferSize  ( BUFFER_LENGTH )
    ->transferType  ( MODDMA::p2m )
	  ->transferWidth ( MODDMA::halfword ) //ARM : Word size is 32-bits
    ->srcConn       ( MODDMA::ADC )
    ->dstConn       ( 0 )
    // When done, we goto lliB
    ->dmaLLI        ( (uint32_t) (&lliB))
    ->attach_tc     ( &TC0_callback )
    ->attach_err    ( &ERR0_callback )
    ; // end conf.

    // Prepare configuration.
    dma.Setup( cfg );

    LPC_GPDMACH_TypeDef *pChannel = (LPC_GPDMACH_TypeDef *)dma.Channel_p( cfg->channelNum() );
	/* the DMA Setup Above - assumes
	 * the ADC is a 32bit register
	 * Yes it is, but the data is in the low
	 * 16bits and that is all we want
	 * so - we change the control register bits
	 */

		 
		lliA.srcAddr( pChannel->DMACCSrcAddr );
    lliB.srcAddr( pChannel->DMACCSrcAddr );
     
    lliA.dstAddr( (uint32_t)(BUFA_START) );
    lliB.dstAddr( (uint32_t)(BUFB_START) );
     
    lliA.nextLLI( (uint32_t)(&lliB) );
    lliB.nextLLI( (uint32_t)(&lliA) );
     
    lliA.control( pChannel->DMACCControl + BUFFER_LENGTH );
    lliB.control( pChannel->DMACCControl + BUFFER_LENGTH );
}

void setup_clocks(void)
{   
    uint32_t v;

		/* ADC clock needs to be 1/4 of the PCLK
		 * 93.6mhz / 8 = 11.7mhz
		 * Given 65 ADC clocks = 180khz
		 * which is exactly 1.5 times our signal rate
		 */
		
    v = LPC_SC->PCLKSEL0; 
    v &= ~(0x3 << 24);
    v |= (0x3 << 24);
    LPC_SC->PCLKSEL0 = v;

}

void setup_adc(void)
{
    uint32_t v;
    LPC_PINCON->PINSEL3 &= ~(3UL << 30);  /* P1.31, Mbed p20. */
    LPC_PINCON->PINSEL3 |=  (3UL << 30);


    /* Power up the ADC */
    LPC_SC->PCONP    |=  (1UL << 12);

    /* The PCLOCK = 93.6mhz
     * The input to the ADC must be less then 13mhz
     * So we divide by 8 - giving 11.7mhz
     */

    /* build the ADC Control Register */    
    v = 0;
    /* select channel 5 bits[7:0]*/
    v |= (1 << 5);
    /* Clock Divisor, bits[15:8] */
    /*  GIVEN: 93.6mhz peripheral clock 
     * DIVIDE:  8
     * RESULT: 11.7mhz ADC clock
		 * ADC requires 65 clocks per conversion
		 * Which gives us a 180khz sample rate
     */
    int adc_clock_div = 1;
    v |= ((adc_clock_div - 1 ) << 8 );
    /* Burst Mode bit[16]*/
    v |= (1 << 16);
    /* reserved zero, bits[20:17] */
    v |= 0;
    
    /* Enable the ADC bit[21] */
    v |= (1UL << 21); 
    /* bits[23:22] = reserved, zero */
    v |= 0;
    /* Bits[26:24] = No start setting */
    v |= 0;
    /* MUST BE zero = See data sheet and below */
    
    /* bits[27] = not applicable */
    v |= 0;
    /* bits[31:28] = reserved zero */
    v |= 0;
    LPC_ADC->ADCR =  v;

    /* interrrupt enable: ADGINTEN = 0 */
    /* From the data sheet:
     * Remark: START bits must be 000 when BURST = 1 
     * or conversions will not start. If BURST is set to 1, 
     * the ADGINTEN bit in the AD0INTEN register 
     * (Table 534) must be set to 0.
     */
    LPC_ADC->ADINTEN = 0x100;
}



static void reset_decoder(void)
{
	state_counter = 0;
	decode_state = '?';
	am_h_counter = 0;
	am_l_counter = 0;
	max_mag_seen = -(1<<20);
	min_mag_seen = +(1<<20);
	memset( (void *)(&sixty_symbols[0]), 0, sizeof(sixty_symbols) );
}




static void low_case(void)
{
	AM_LED = 0;
	am_l_counter++;
}

static void high_case(void)
{
	AM_LED = 1;
	am_h_counter++;
}

static void rising_case(void)
{
	am_h_counter++;
	AM_LED = 1;
}


static void falling_case(void)
{
	/* we are at the end of the AM signal */
	int msecs_high;
	
	msecs_high = (am_h_counter * 1000);
	msecs_high = msecs_high / (am_l_counter+ am_h_counter);
	//debug_printf("am=%dmSecs\r\n", msecs_high);
	am_h_counter = 0;
	am_l_counter = 1;
}

void decoding_state(void)
{
	int new_state;
	if( prev_am_level == 'L' ){
		if( this_am_level == 'L' ){
			new_state = 'L';
		} else {
			new_state = 'R';
		}
	} else {
		if( this_am_level == 'L' ){
			new_state = 'F';
		} else {
			new_state = 'H';
		}
	}
	
	if( new_state != decode_state ){
		state_counter = 0;
	}
	
	decode_state = new_state;
		
	switch( new_state ){
	case 'H':
		high_case();
		break;
	case 'L':
		low_case();
		break;
	case 'R':
		rising_case();
		break;
	case 'F':
		falling_case();
		break;
	}
		
	
	if( state_counter <  SAMPLES_nSECS(3) ){
		return;
	}
	debug_printf("STUCK!\r\n");
	/* We have been stuck at the same 
	 * symbol for far too long we have 
	 * no signal this is bad - maybe
	 * maybe our AM threshholds are bad?
	 */
	reset_decoder();
}



static void hunting_state(void)
{
	int x;
	state_counter++;
	if( state_counter < SAMPLES_nSECS(3) ){
		x = !!(state_counter & 8);
	} else {
		x = 0;
		decode_state = 'D';
		debug_printf("Begin!\r\n");
	}
	
	AM_LED = x;
	PH_LED = x;
	HB_LED = x;
  IRQ_LED = x; 
}

static void threshold_data(void)
{
	int x;
	x = this_sample.mag8bit;
	
	dbg_hist[ dbg_hist_index ] = this_sample;
	dbg_hist_index = (dbg_hist_index+1) % DBG_HIST_SIZE;
	if( dbg_hist_index == 0 ){
		dbg_halt = 1;
	}
	
	/* Apply a bit of hysterisis 
	 * for the magnitude changes
	 *
	 * If we are high, we must see
	 * something below 1/3 signal
	 *
	 * If we are low, we must see 
	 * something above 2/3 signal
	 */
	if( prev_am_level == 'H' ){
		if( x < (255/3) ){
			this_am_level = 'L';
		} else {
			this_am_level = 'H';
		}
	} else {
		/* low */
		if( x > ((2*255)/3) ){
			this_am_level = 'H';
		} else {
			this_am_level = 'L';
		}
	}
}
			 
   
int main()
{	
    // Setup the serial port to print out results.
		// this is the only time we use this
    DONOT_USE_THIS.baud(9600);
		//angle_test();
	
		debug_printf("ADC with DMA example\r\n");
    debug_printf("===================\r\n");
	
    // We use the ADC irq to trigger DMA and the manual says
    // that in this case the NVIC for ADC must be disabled.
    NVIC_DisableIRQ(ADC_IRQn);

    setup_clocks();

    setup_adc();

    setup_dma();

    // Enable configuration.
		active_buffer = 'A';
    dma.Enable( &(dmacfg) );
  
    while (1) {
			
			debug_background_hook();
			if( dbg_halt ){
				break;
			}
		}
		
		
		dma.Disable( ADC_CHNL_NUM ) ;
		LPC_ADC->ADCR |= ~(1UL << 16);
    LPC_ADC->ADINTEN = 0;
		
		int x;
		for( x = 0 ; x < DBG_HIST_SIZE ; x++ ){
			DONOT_USE_THIS.printf("%d,%d\r\n", 
			dbg_hist[x].mag8bit, dbg_hist[x].bin_angle );
		}
		debug_printf("Bye Bye\n");
}




// Configuration callback on TC
void TC0_callback(void)
{
	int x;
	IRQ_LED = 1;
  x = totalComplete;
  x = x + 1;
  totalComplete = x;
	//BUFFER_LED = x & 1;
	
	process_samples(active_buffer);

	threshold_data();
	
	switch( decode_state ){
	default:
		reset_decoder();
		break;
	case '?':
		hunting_state();
		break;
	case 'D':
	case 'H':
	case 'L':
	case 'R':
	case 'F':
		decoding_state();
		break;
	}
	/* now switch the buffer letter */
  // we start with "A" = 0x41
	// Binary:  0100 0001
  //    XOR:  0000 0011
	// ===================
	//          0100 0010 = 0x62 (B)
	//                 11
	//          = 65 = our A
	// Do it again, and we get B
	active_buffer = active_buffer ^ 3;
	IRQ_LED = 0;
}

// Configuration callback on Error
void ERR0_callback(void)
{
    // Switch off burst conversions.
    LPC_ADC->ADCR |= ~(1UL << 16);
    LPC_ADC->ADINTEN = 0;
    dma.clearErrIrq();
    error("Oh no! My Mbed EXPLODED! :( Only kidding, go find the problem");
}
