/*
 * dmod.cpp
 *
 *  Created on: May 22, 2015
 *      Author: duaneellis
 */

#include "decode.h"

/* this calculates the angle in degress */
int get_degree_angle( int x, int y)
{
	int a;

	a = 0;

	if( y < 0 ){
		/* angle is in quadrent 3 or 4 */
		/* rotate 180 degrees */
		a = get_degree_angle( -x, -y );
		a = a + 180;
		return a;
	}
	/* We are in quadrent 1 or 2 */
	if( x < 0 ){
		/* angle is in quadrent 2 */
		/* Rotate 90 degrees */
		a = get_degree_angle(y, -x );
		a = 90 + a;
		return a;
	}

	//===========================
	// We are in quadrent 1
	// Both X and Y are positive.
	//===========================

	// Are we above or below 45 degrees
	if( y > x ){
		// Rotate (mirror) around 45 degrees
		a = get_degree_angle( y, x );
		a = 90 - a;
		return a;
	}

	//====================================
	// At this point, we are in quadrent 1.
	// And y is less then equal x
	// Meaning: Between 0 and 45 degrees
	//====================================

	//====================================
	// We need to calculate tangent.
	// Which will be a floating point number
	// between 0 and 1, but we do not want
	// any floating point calculations
	//====================================

	// So what we want is:  1000 * (y/x)

	// simplify do not divide by zero below
	if( x == 0 ){
		return 0;
	}

	//====================================
	// Now normalize x & y to +/-32768
	// we use 16384 to leave for sign bit
	// which will be located at bit 15
	//====================================

	// From above, we know that X is larger.
	// Normalize loop, scale down first
	while( x > (32768-1) ){
		x = x >> 1;
		y = y >> 1;
	}

	// if X is too small, scale up
	while( x < 16834 ){
		x = x << 1;
		y = y << 1;
	}

	//========================================
	// Now tangent is (something) / (about 16834)
	// And (something) is less then 16384
	//========================================

	// We can now do this:
	//
	//   (something * 1024) / (about 16384)
	//
	// And get our integer tangent!
	//
	int tangent;

	tangent = (y*1024)/x;

	/* convert tangent to an angle */
	/* simple lookup table */
#define DO_TEST( VAL, ANGLE ) \
	if( tangent < (VAL) ){ return ANGLE; }

	DO_TEST(	26	,	0	);
	DO_TEST(	80	,	3	);
	DO_TEST(	134	,	6	);
	DO_TEST(	189	,	9	);
	DO_TEST(	245	,	12	);
	DO_TEST(	303	,	15	);
	DO_TEST(	362	,	18	);
	DO_TEST(	424	,	21	);
	DO_TEST(	488	,	24	);
	DO_TEST(	555	,	27	);
	DO_TEST(	627	,	30	);
	DO_TEST(	703	,	33	);
	DO_TEST(	785	,	36	);
	DO_TEST(	874	,	39	);
	DO_TEST(	971	,	42	);
	DO_TEST(	1079	,	45	);
	/* just in case this goes wrong! */
	return 45;
}

static int get_binary_angle( int s, int c)
{
	int a;
	a = get_degree_angle( s,c );
	a = a * 255;
	a = a / 360;
	return a;
}



void process_samples(int bufletter)
{
	int avg_sum;
	int mag;
	int sumA,sumB,sumC;
	int DCsample;
	int ACsample;
	int ABSsample;
	int mask;
	uint32_t v;
	const uint32_t *data;
	const uint32_t *data_end;


	mask = 0x0000ffff;
	data = NULL;
	if( bufletter == 'A' ){
		data = BUFA_START;
		/* we need to copy the previous avg history
		** window to the start of the data buffer
		**/

		memcpy( (void *)BUF_RAW_START, (void *)BUFB_AVG_END, AVG_LENGTH_BYTES );
	} else {
		data = BUFB_START;
	}
	data_end = data + BUFFER_LENGTH;

	/* pick up our old average */
	avg_sum = old_avg_sum;
	/* clear our sums */
	sumA = sumB = sumC = 0;
	/* clear our magnitude */
	mag = 0;
	/* process samples in sets of 3 */
	while( data != data_end ){

#define DO_SAMPLE( XX ) 				\
		DCsample = (*data & mask);	/* FETCH the sample */	\
		/* update average: Remove old sample, add new sample */			\
		avg_sum = avg_sum + DCsample - (data[ -AVG_LENGTH ] & mask); 	\
		data++; /* point at next sample (for next entry) */	\
		/* simulate an AC coupled input */			\
		ACsample = DCsample - (avg_sum / AVG_LENGTH );	\
		XX += ACsample;		/* add our sum */									\
		ABSsample = ACsample; 	/* calculate magnitude */		\
		if( ABSsample < 0 ){							\
			ABSsample = -ABSsample;					\
		}																	\
		mag += ABSsample

		DO_SAMPLE( sumA );
		DO_SAMPLE( sumB );
		DO_SAMPLE( sumC );
	}

	/* Keep the average for the next
	** time we process a buffer.
	*/
	old_avg_sum = avg_sum;

	/* The ADC is 12bits, the data is in
	** bits[15:4], bits[3:0] are zero.
	** Thus we can think of the magnitude
	** as a 16bit number * BUFFER_LENGTH
	*/

	mag = mag / (16 * BUFFER_LENGTH_LOWER_LOG2);
	/*
	** We need some type of automatic
	** gain control but don't realy have
	** one, so we do a poor mans solution
	** we track the max magnitude
	*/
	if( mag < min_mag_seen ){
		min_mag_seen = mag;
	}

	if( mag > max_mag_seen ){
		max_mag_seen = mag;
	}
	/* Now scale our magnitude to 0..250 */
	/* Why 250? We do not want to overflow */
	mag = mag - min_mag_seen;
	mag = mag * 250;
	mag = mag / (max_mag_seen - min_mag_seen);
	/* now we have our magnitude! */
	this_sample.mag8bit = mag;

	/*=============================
	** Now - we must calculate sin & cos
	** also known as the I and Q signal.
	**=============================
	** our fake local oscilator has 3 sample
	**  points, the points are at these angles
	**
	**  Func |  0deg | 120deg   | 240deg
	**          A       B          C
	**  -----+-------+----------+----------+
	**  cos  |  1    |  -0.5    | -0.5     |
	**  sin  |  0    | +0.86602 | -0.86602 |
	**  -----+-------+----------+----------+
	** Below, we make "1=1024" and 0.5=512, because this
	** becomes a simple shift, instead a multily
	** The +0.86602 sadly is a multiplication
	**
	** thus 1 = 1024, +/-0.5 = +/-512
	** and  +/-0.86602 = (1024 * 0.86602) = 886.8 => 887
	**
	**===========
	** Second problem - our values can be huge or
	** the values can be small, we need to normalize
	** our sin and cosine values in a reasonable way
	**
	** To do this, we  scale/normalize sumA,sumB and suMC
	** We need to do this before we multiply by our angles.
	** otherwise the numbers overflow 32bits :-(
	**===================
	**
	** Here is how we normlaize
	**
  ** For example if our value is 3, (binary 011)
	** Bit 2 is our 'effective sign bit'
	**
	** In comparison, 127, bit 7 is the sign.
	**
	** THUS:
	**
	** for all positive numbers the sign bit is
	** effectively one more then the highest bit
	**
	** We have to deal with negative numbers
	** but that is easy, we just do a ones compliment
	**
	** We then bitwise OR all of the values together
	** into a single value, and work with that.
	**
	** We use that result to normalize.
	*/

	 /* combine the values */
	 v = 0;
#define funny_absolute_value( VAL ) \
	((VAL < 0) ? (~(uint32_t)(VAL)) : (VAL))
	v |= funny_absolute_value(sumA);
	v |= funny_absolute_value(sumB);
	v |= funny_absolute_value(sumC);

	/* our values are all positives, ored
	** together, the sign bit is +1 bit past
	** the most significant set bit
  **
  ** We can fix this by mulitpling V by 2
  ** Thus, the the MSB bit set is now the
  ** sign bit
	*/
	 v = v * 2;

	/* Now we can scale our sin and cos
  ** to any value we choose, and we will
	** choose +/- 1024 as our goal number
  **/

	/* CASE 1 - The number is too small */
	while( v < 1024 ){
		v = v << 1;
		sumA = sumA << 1;
		sumB = sumB << 1;
		sumC = sumC << 1;
	}

	/* CASE 2 - the number is too big */
	while( v > 1024 ){
		v = v >> 1;
		/* we need to do this as a signed shift
		** not as division, the reason is this
		** Consider dividing by 2, the sequence
		** of negative numbers is:  -2, -1, 0
		** But shifting gives: -4, -2, -1, -1
		*/
		sumA = sumA >> 1;
		sumB = sumB >> 1;
		sumC = sumC >> 1;
	}

#define COS_A  (1024) /* 1024 * 1 */
#define COS_B  (-512) /* 1024 * -0.5 */
#define COS_C  (-512) /* 1024 * -0.5 */
#define SIN_A  (0)
#define SIN_B  (+887) /* 1024 * +0.86602 rounded */
#define SIN_C  (-887) /* 1024 * -0.86602 rounded */

	/* our values are normalized to 1024
  ** We now multiply by SIN and COS, which are
	** also scaled to exacty 1024
	**
	** Because we are going to add 3 data points
	** We could have: 3 * 1024 * 1024 = 3 million
	** which is a 22 bit number, 10 bits to spare
  ** Well within our 32bit number :-)
	**/

	int v_cos, v_sin;
	v_cos = (COS_A * sumA) + (COS_B * sumB) + (COS_C * sumC);
	v_sin = (SIN_A * sumA) + (SIN_B * sumB) + (SIN_C * sumC);

	/* We need to get our integer tangent value
	** and convert this to a phase angle.
	**
	** At 45 degrees, tangent is 1
	** Less then 45 - it deminishes to zero
	** More then 45 - it grows to infinity
	**
	** Our first goal is to rotate the angle
	** to the first quadrent.
	*/

	this_sample.bin_angle = get_binary_angle( v_sin, v_cos );
}
