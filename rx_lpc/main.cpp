#define GLOBAL
#include "decode.h"

int dbg_halt = 0;
#if 1
DigitalOut AM_LED(P0_22);
#else
int AM_LED;
#endif

#if 0
DigitalOut PH_LED(LED2);
#else
int PH_LED;
#endif

#if 0
DigitalOut IRQ_LED(P0_22);
#else
int IRQ_LED;
#endif

#if 0
DigitalOut HB_LED(LED4);
#else
int HB_LED;
#endif




MODDMA dma;
MODDMA_LLI lliA;
MODDMA_LLI lliB;
MODDMA_Config dmacfg;

// #define ADC_DATA_REGISTER ((uint32_t)(&LPC_ADC->ADGDR))
#define ADC_DATA_REGISTER ((uint32_t)(&(LPC_ADC->ADDR5)))

// Function prototypes for IRQ callbacks.
// See definitions following main() below.
void TC0_callback(void);
void ERR0_callback(void);

#define ADC_CHNL_NUM  MODDMA::Channel_0

void setup_dma( void )
{
	MODDMA_Config *cfg;
	// Setup active DMA configuration
	// we start by setting it Buffer A
	cfg = &(dmacfg);
	cfg->channelNum( ADC_CHNL_NUM )
    		->srcMemAddr    ( ADC_DATA_REGISTER )
			->dstMemAddr    ( (uint32_t)(BUFA_START))
			->transferSize  ( BUFFER_LENGTH )
			->transferType  ( MODDMA::p2m )
			->transferWidth ( MODDMA::halfword ) //ARM : Word size is 32-bits
			->srcConn       ( MODDMA::ADC )
			->dstConn       ( 0 )
			// When done, we goto lliB
			->dmaLLI        ( (uint32_t) (&lliB))
			->attach_tc     ( &TC0_callback )
			->attach_err    ( &ERR0_callback )
			; // end conf.

	// Prepare configuration.
	dma.Setup( cfg );

	LPC_GPDMACH_TypeDef *pChannel = (LPC_GPDMACH_TypeDef *)dma.Channel_p( cfg->channelNum() );
	/* the DMA Setup Above - assumes
	 * the ADC is a 32bit register
	 * Yes it is, but the data is in the low
	 * 16bits and that is all we want
	 * so - we change the control register bits
	 */


	lliA.srcAddr( pChannel->DMACCSrcAddr );
	lliB.srcAddr( pChannel->DMACCSrcAddr );

	lliA.dstAddr( (uint32_t)(BUFA_START) );
	lliB.dstAddr( (uint32_t)(BUFB_START) );

	lliA.nextLLI( (uint32_t)(&lliB) );
	lliB.nextLLI( (uint32_t)(&lliA) );

	lliA.control( pChannel->DMACCControl + BUFFER_LENGTH );
	lliB.control( pChannel->DMACCControl + BUFFER_LENGTH );
}

void setup_clocks(void)
{
	uint32_t v;

	/* ADC clock needs to be 1/4 of the PCLK
	 * 93.6mhz / 8 = 11.7mhz
	 * Given 65 ADC clocks = 180khz
	 * which is exactly 1.5 times our signal rate
	 */

	v = LPC_SC->PCLKSEL0;
	v &= ~(0x3 << 24);
	v |= (0x3 << 24);
	LPC_SC->PCLKSEL0 = v;

}

void setup_adc(void)
{
	uint32_t v;
	LPC_PINCON->PINSEL3 &= ~(3UL << 30);  /* P1.31, Mbed p20. */
	LPC_PINCON->PINSEL3 |=  (3UL << 30);


	/* Power up the ADC */
	LPC_SC->PCONP    |=  (1UL << 12);

	/* The PCLOCK = 93.6mhz
	 * The input to the ADC must be less then 13mhz
	 * So we divide by 8 - giving 11.7mhz
	 */

	/* build the ADC Control Register */
	v = 0;
	/* select channel 5 bits[7:0]*/
	v |= (1 << 5);
	/* Clock Divisor, bits[15:8] */
	/*  GIVEN: 93.6mhz peripheral clock
	 * DIVIDE:  8
	 * RESULT: 11.7mhz ADC clock
	 * ADC requires 65 clocks per conversion
	 * Which gives us a 180khz sample rate
	 */
	int adc_clock_div = 1;
	v |= ((adc_clock_div - 1 ) << 8 );
	/* Burst Mode bit[16]*/
	v |= (1 << 16);
	/* reserved zero, bits[20:17] */
	v |= 0;

	/* Enable the ADC bit[21] */
	v |= (1UL << 21);
	/* bits[23:22] = reserved, zero */
	v |= 0;
	/* Bits[26:24] = No start setting */
	v |= 0;
	/* MUST BE zero = See data sheet and below */

	/* bits[27] = not applicable */
	v |= 0;
	/* bits[31:28] = reserved zero */
	v |= 0;
	LPC_ADC->ADCR =  v;

	/* interrupt enable: ADGINTEN = 0 */
	/* From the data sheet:
	 * Remark: START bits must be 000 when BURST = 1
	 * or conversions will not start. If BURST is set to 1,
	 * the ADGINTEN bit in the AD0INTEN register
	 * (Table 534) must be set to 0.
	 */
	LPC_ADC->ADINTEN = 0x100;
}



static void reset_decoder(void)
{
	stuck_counter = 0;
	am_h_counter = 0;
	am_l_counter = 0;
	max_mag_seen = -(1<<20);
	min_mag_seen = +(1<<20);
	memset( (void *)(&sixty_symbols[0]), 0, sizeof(sixty_symbols) );
}


static void falling_case(void)
{
	/* we are at the end of the AM signal */
	int msecs_high;
	int am_signal;
	int msecs_total;

	msecs_total = am_l_counter + am_h_counter;
	msecs_total = msecs_total * MSECS_PER_SAMPLE;
	if( (msecs_total < 900) || (msecs_total > 1100) ){
		am_signal = 4;
		goto done;
	}

	msecs_high = (am_h_counter * 1000);
	/*
	 *  AM Signals these durations
	 *  200 / 800 = Marker
	 *  500 / 500 = Logic 1
	 *  800 / 200 = Logic 0
	 *
	 *  500 - 200 = 300
	 *  Split that we have 150
	 *
	 *  Thus, MARKER/ONE threshold is 350msecs
	 *  Our   ONE/ZERO threshold is 650 msecs
	 */
	if( msecs_high < 350 ){
		am_signal = 2;
	} else if( msecs_high > 650 ){
		am_signal = 0;
	} else {
		am_signal = 1;
	}
done:
	am_h_counter = 0;
	am_l_counter = 1;

	/* SHIFT data over */
	memmove( (void *)(&(sixty_symbols[0])),
			 (void *)(&(sixty_symbols[1])),
			 sizeof(sixty_symbols)-sizeof(sixty_symbols[0]) );

	sixty_symbols[59].am_data = am_signal;
	sixty_symbols[59].ph_data = current_phase_signal;
	/* the phase code will insert the phase data */
	decode_dump_flag = 1;
}

static void ph_decode(void)
{
	int this_angle;
	int delta_angle;
static int last_angle;

	this_angle = this_sample.bin_angle;

	if( (this_angle < 64) && (last_angle > 192) ) {
		// Rotating clockwise, crossing zero degrees
		delta_angle = this_angle + 256 - last_angle;
	} else if( (this_angle > 192) && (last_angle < 64) ){
		// Rotating counter clockwise crossing zero degrees
		delta_angle = this_angle - (last_angle + 256);
	} else {
		/* does not matter which direction CW or CCW
		 * we are no crossing zero so not special case
		 */
		delta_angle = this_angle - last_angle;
	}

	last_angle = this_angle;

	/*
	 * We are decoding BPSK here
	 * We have an average angle
	 * for the last 10 milliseconds
	 *
	 * It has some rotation, due to
	 * frequency offset error, it could
	 * be as much as 3hertz error
	 *
	 * Within this 10millisecond period
	 * That translates into (3*360=960)
	 * degrees per second rotation
	 *
	 * Or about 10 degrees per sample.
	 *
	 * Or: (10*256/360) about 7.1 using the
	 * binary angle method
	 *
	 * If we have a true phase shift
	 * We should see about 128 binary angle
	 * degrees, but sadly we filter alot
	 * so it will not be as sharp
	 *
	 * We'll pick 64 as our decision point
	 * Meaning if we see an angular shift of
	 * more then 64 binary degrees we have a phase shift
	 */
	if( delta_angle < 0 )
		delta_angle = -delta_angle;

	if( delta_angle > 64 ){
		/* We have a phase shift */
		current_phase_signal = !current_phase_signal;
	}
}

static void same_case(void)
{
	stuck_counter++;
	if( stuck_counter < SAMPLES_nSECS(2) ){
		return;
	}

	debug_printf("AM Signal stuck reseting\n");
	reset_decoder();
}


void am_decode(void)
{
	int x;
	x = this_sample.mag8bit;

	/* Apply a bit of hysteresis
	 * for the magnitude changes
	 *
	 * If we are high, we must see
	 * something below 1/3 signal
	 */
#define ONE_THIRD ((250 * 1)/3)
	 /*
	 * If we are low, we must see
	 * something above 2/3 signal
	 */
#define TWO_THIRD ((250 * 2)/3)

	if( prev_am_level == 'H' ){
		if( x < ONE_THIRD ){
			this_am_level = 'L';
		} else {
			this_am_level = 'H';
		}
	} else {
		/* low */
		if( x > TWO_THIRD ){
			this_am_level = 'H';
		} else {
			this_am_level = 'L';
		}
	}

	/* what happened? */

	if( prev_am_level == this_am_level ){
		same_case();
		return;
	}

	stuck_counter = 0;
	if( this_am_level == 'H' ){
		am_h_counter++;
		return;
	}
	falling_case();
}


void decode_this_sample(void)
{
	ph_decode();
	am_decode();
}

// Configuration callback on TC
void TC0_callback(void)
{
	int x;
	IRQ_LED = 1;
	x = totalComplete;
	x = x + 1;
	totalComplete = x;
	//BUFFER_LED = x & 1;

	process_samples(active_buffer);

	decode_this_sample();

	/* now switch the buffer letter */
	// we start with "A" = 0x41
	// Binary:  0100 0001
	//    XOR:  0000 0011
	// ===================
	//          0100 0010 = 0x62 (B)
	//                 11
	//          = 65 = our A
	// Do it again, and we get B
	active_buffer = active_buffer ^ 3;
	IRQ_LED = 0;

	dbg_hist[ dbg_hist_index ] = this_sample;
	dbg_hist_index = (dbg_hist_index+1) % DBG_HIST_SIZE;
	if( dbg_hist_index == 0 ){
		//dbg_halt = 1;
	}


}

// Configuration callback on Error
void ERR0_callback(void)
{
	// Switch off burst conversions.
	LPC_ADC->ADCR |= ~(1UL << 16);
	LPC_ADC->ADINTEN = 0;
	dma.clearErrIrq();
	error("Oh no! My Mbed EXPLODED! :( Only kidding, go find the problem");
}


void dump_decode_data(void)
{
	int x;
	for( x = 0 ; x < 60 ; x++ ){
		debug_printf("%d", sixty_symbols[x].am_data);
	}
	debug_printf("\r\n");
	for( x = 0 ; x < 60 ; x++ ){
		debug_printf("%d", sixty_symbols[x].ph_data);
	}
	debug_printf("\r\n");
	debug_printf("\r\n");

}

void real_decoding(void)
{
	// Enable configuration.
	active_buffer = 'A';
	dma.Enable( &(dmacfg) );

	int last_status = -1;
	int this_status = 1234;
	while (1) {

		/* we need to service the serial port buffer */
		debug_background_hook();
		if( dbg_halt ){
			/* For debug reasons we might want to halt
			 * and print our debug buffers
			 */
			break;
		}

		/* print how many times we have captured data */
		this_status = totalComplete / 100;
		if( this_status != last_status ){
			last_status = this_status;
			debug_printf("%d sec\r\n", totalComplete);
		}
		if( decode_dump_flag ){
			dump_decode_data();
		}
	}
}


int main()
{
	int x;

	// Setup the serial port to print out results.
	// this is the only time we use this
	DONOT_USE_THIS.baud(115200);
	//angle_test();

	debug_printf("ADC with DMA example\r\n");
	debug_printf("===================\r\n");

	// We use the ADC irq to trigger DMA and the manual says
	// that in this case the NVIC for ADC must be disabled.
	NVIC_DisableIRQ(ADC_IRQn);

	setup_clocks();

	setup_adc();

	setup_dma();

#if 1
	real_decoding();
#else
	test_decoding();
#endif

	dma.Disable( ADC_CHNL_NUM ) ;
	LPC_ADC->ADCR |= ~(1UL << 16);
	LPC_ADC->ADINTEN = 0;


	for( x = 0 ; x < DBG_HIST_SIZE ; x++ ){
		DONOT_USE_THIS.printf("%5d, %4d, %4d\r\n",
				x,
				dbg_hist[x].mag8bit,
				dbg_hist[x].bin_angle);
	}
	debug_printf("Bye Bye\n");
}

