################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../angle_test.cpp \
../bg_serial.cpp \
../decode_test_data.cpp \
../dmod.cpp \
../main.cpp 

C_SRCS += \
../system_lpc176x.c 

OBJS += \
./angle_test.o \
./bg_serial.o \
./decode_test_data.o \
./dmod.o \
./main.o \
./system_lpc176x.o 

C_DEPS += \
./system_lpc176x.d 

CPP_DEPS += \
./angle_test.d \
./bg_serial.d \
./decode_test_data.d \
./dmod.d \
./main.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C++ Compiler'
	arm-none-eabi-c++ -D__NEWLIB__ -D__CODE_RED -DCPP_USE_HEAP -DTARGET_LPC1768 -DTARGET_M3 -DTARGET_CORTEX_M -DTARGET_NXP -DTARGET_LPC176X -DTARGET_MBED_LPC1768 -DTOOLCHAIN_GCC_CR -DTOOLCHAIN_GCC -D__CORTEX_M3 -DARM_MATH_CM3 -DMBED_BUILD_TIMESTAMP=1432352557.99 -D__MBED__=1 -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/MODDMA" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TOOLCHAIN_GCC_CR" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TARGET_NXP" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TARGET_NXP/TARGET_LPC176X" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TARGET_NXP/TARGET_LPC176X/TARGET_MBED_LPC1768" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -mcpu=cortex-m3 -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__NEWLIB__ -D__CODE_RED -DCPP_USE_HEAP -DTARGET_LPC1768 -DTARGET_M3 -DTARGET_CORTEX_M -DTARGET_NXP -DTARGET_LPC176X -DTARGET_MBED_LPC1768 -DTOOLCHAIN_GCC_CR -DTOOLCHAIN_GCC -D__CORTEX_M3 -DARM_MATH_CM3 -DMBED_BUILD_TIMESTAMP=1432352557.99 -D__MBED__=1 -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/MODDMA" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TOOLCHAIN_GCC_CR" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TARGET_NXP" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TARGET_NXP/TARGET_LPC176X" -I"/Users/duaneellis/LPCXpresso/workspace/mbed_blinky/mbed/TARGET_LPC1768/TARGET_NXP/TARGET_LPC176X/TARGET_MBED_LPC1768" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -fno-exceptions -fno-rtti -mcpu=cortex-m3 -mthumb -D__NEWLIB__ -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


