// Toggle the blinking led after 5 seconds
#include <stdint.h>
#include "mbed.h"
#include "rtos.h"
#include <time.h>
#include <string>
#include <vector>
#include "ClockControl/ClockControl.h"
//#include "GetTickCount/GetTickCount.h"

// Peripheral
Serial pc(USBTX, USBRX); // tx, rx
AnalogOut aout(p18);

// Resolution functions
const int RESOLUTION = 32;
const float PI = 3.14159265359;
uint16_t VH[RESOLUTION];
uint16_t VL[RESOLUTION];
uint16_t NVH[RESOLUTION];
uint16_t NVL[RESOLUTION];


unsigned char voltage[60][10] = {0,{9}};

// Timer
Ticker ticker1;
Ticker ticker2;


// LED
DigitalOut led1(LED1);
DigitalOut led2(LED2);


void generate_sin_table()
{
    float T = 1.0/((float)RESOLUTION);
    float t = 0;
    int j = RESOLUTION/2; // Resolution is 32 start from 16 for phase shifted sine waves
    for (int i = 0; i < RESOLUTION; i++) {
        // Need 0-1v swing for AnalogOut (+0.5)
        VH[i] = (uint16_t) 65535 * (0.5 * sin(2 * PI * t) + 0.5);
        VL[i] = (uint16_t) (9362 * (0.5 * sin(2 * PI * t) + 0.5)) + 32767 - 4681;
        if ( (i+j) < RESOLUTION ) {
            NVH[i+j] = VH[i];
            NVL[i+j] = VL[i];
        } else {
            NVH[(i%j)] = VH[i];
            NVL[(i%j)] = VL[i];
        }


        //  pc.printf("%x\n", VH[i]);
        t += T;
    }
    /*
        for(int i =0; i <RESOLUTION; i++) {
            pc.printf("[%d] ", VH[i]);
            //pc.printf(" VL[%d] NVL[%d]", VL[i], NVL[i]);
        }

        pc.printf("\n");

        for(int i =0; i <RESOLUTION; i++) {
             pc.printf("[%d] ", NVH[i]);
            //pc.printf(" VL[%d] NVL[%d]", VL[i], NVL[i]);
        }
        */
}

float amp1 = 0;
float amp2 = 0.303;
void writeClock()
{
    static uint8_t logic = 0;
    if (logic == 0) {
        aout.write(amp1);
        logic = 1;
    } else if (logic == 1) {
        aout.write(amp2);
        logic = 0;
    }
}

void analogWave()
{
    static uint8_t index = 0;
    aout.write_u16(VH[index&0x03]);
    index++;
}

void change()
{
    static uint8_t i = 0;
    if (i == 0) {
        amp1 = 0;
        amp2 = 1;
        i = 1;
    } else if (i == 1) {
        amp1 = 1;
        amp2 = 0;
        i = 2;
    } else if (i == 2) {
        amp1 = 0;
        amp2 = 0.303;
        i = 3;
    } else if (i == 3) {
        amp1 = 0.303;
        amp2 = 0;
        i = 0;
    }

}

int chg;
void change2()
{
    static uint8_t i = 0;
    if (i == 0) {
        chg = 0;
        i = 1;
    } else if (i == 1) {
        chg = 1;
        i = 2;
        //i=0;
    } else if ( i == 2) {
        chg = 2;
        i = 3;
    } else {
        chg  = 3;
        i = 0;
    }
}


void convertDate (int &min,  int &hour, int &day, int &year, string time_str)
{
    size_t i;
    i = time_str.find_first_of(":");
    min = atoi( time_str.substr(0, i).c_str());
    pc.printf("temp %s\n", time_str.substr(0, i).c_str());
    time_str = time_str.substr(i+1, time_str.size());

    i = time_str.find_first_of(":");
    hour = atoi( time_str.substr(0, i).c_str() );
    time_str = time_str.substr(i+1, time_str.size());

    i = time_str.find_first_of(":");
    day = atoi( time_str.substr(0, i).c_str() );
    time_str = time_str.substr(i+1, time_str.size());

    i = time_str.find_first_of(":");
    year = atoi( time_str.substr(0, i).c_str() );
    time_str = time_str.substr(i+1, time_str.size());
}

//NIST Date format buffers
#define SIZE_DATE_BUFFER 26 // 26  bit date word
#define SIZE_SYNCT_BUFFER 13 //synchronization word length
#define SIZE_PM_PACKET 60
unsigned char pm_packet[SIZE_PM_PACKET] = {0};
static unsigned char date_buffer[SIZE_DATE_BUFFER] = {0};
static unsigned char sync_T[SIZE_SYNCT_BUFFER] = {0,0,0,1,0,1,1,0,1,1,1,0,0}; //Time synchronization word
static unsigned char dst_ls[5] = {0};
static unsigned char dst_next[5] = {0};
static unsigned char time_par[5] = {0};
static unsigned char am_packet[SIZE_PM_PACKET] = {0};


// Time variables
static time_t minuteCountStart = 0;
static time_t minuteCounter =0;

void setAmBuffer()
{
    // generate random 0 or 1
    for (int i =0; i <=59; ++i) {
        am_packet[i] = rand()%2;
    }
    am_packet[0] = 2; // 2 = marker
    am_packet[9] = 2;
    am_packet[19] = 2;
    am_packet[29] = 2;
    am_packet[39] = 2;
    am_packet[49] = 2;
    am_packet[59] = 2;

}



void SetMinuteStartCount()
{
    //get the time in seconds for 1st January 2000
    // setup time structure for Wed, 28 Oct 2009 11:35:37
    struct tm t;
    t.tm_sec = 0;    // 0-59
    t.tm_min = 0;    // 0-59
    t.tm_hour = 0;   // 0-23
    t.tm_mday = 1;   // 1-31
    t.tm_mon = 0;     // 0-11 months since Jan
    t.tm_year = 100;  // year since 1900 +100 = 2000

    // convert to timestamp and display (1256729737)
    time_t seconds = mktime(&t);
    //pc.printf("1st January 2000 as seconds since January 1, 1970 = %d\n\r", seconds);
    //convert seconds to minutes for our timestamp calculation
    minuteCountStart = seconds/60;
    //pc.printf("1st January 2000 as minutes since January 1, 1970 = %d\n\r", minuteCountStart);
}

void computeCurrentMinuteCounter()
{
    time_t currentTime;
    time_t t = time(&currentTime); // time in seconds
    //convert to minutes since January 1, 1970
    //currentTime = currentTime/60;
    //compute the current time since 1st jan 2000 as a minute counter.
     //pc.printf("Current Time as minutes since January 1, 2000 t = %d currentTime = %d basic string time = %s\n\r",t, currentTime, ctime(&currentTime));
    minuteCounter = (currentTime/60) - minuteCountStart;
   // pc.printf("Current Time as minutes since January 1, 2000 = %d\n\r",minuteCounter);
}

bool getBit(int i)
{
    int mask = 1;
    mask = mask<<i;
    
    //pc.printf(" Testing bit %d mask %d results %d\n\r", i, mask, (minuteCounter & mask));

    if ( (minuteCounter & mask ) == 0)
        return 0;
    else
        return 1;
}

void createDateBuffer()
{
    // translate minuteCounter into 26 bit time word
    for (int i =0; i < 26; i++) {
        date_buffer[i]= (int)getBit(i);
        //pc.printf(" i %d bit = %d \n\r", i , date_buffer[i]);
    }
}

/*
time_par[0] = sum(modulo 2){time[23, 21, 20, 17, 16, 15, 14, 13, 9, 8, 6, 5, 4, 2, 0]}
time_par[1] = sum(modulo 2){time[24, 22, 21, 18, 17, 16, 15, 14, 10, 9, 7, 6, 5, 3, 1]}
time_par[2] = sum(modulo 2){time[25, 23, 22, 19, 18, 17, 16, 15, 11, 10, 8, 7, 6, 4, 2]}
time_par[3] = sum(modulo 2){time[24, 21, 19, 18, 15, 14, 13, 12, 11, 7, 6, 4, 3, 2, 0]}
time_par[4] = sum(modulo 2){time[25, 22, 20, 19, 16, 15, 14, 13, 12, 8, 7, 5, 4, 3, 1]}
*/
void computeParityBits()
{
    time_par[0] = ( date_buffer[23] + date_buffer[21] + date_buffer[20] + date_buffer[17] + date_buffer[16]
                    + date_buffer[15] + date_buffer[14] + date_buffer[13] + date_buffer[9] + date_buffer[8] + date_buffer[6]
                    + date_buffer[5] + date_buffer[4] + date_buffer[2] + date_buffer[0] )%2;

    time_par[1] = ( date_buffer[24] + date_buffer[22] + date_buffer[21] + date_buffer[18] + date_buffer[17]
                    + date_buffer[16] + date_buffer[15] + date_buffer[14] + date_buffer[10] + date_buffer[9] + date_buffer[7]
                    + date_buffer[6] + date_buffer[5] + date_buffer[3] + date_buffer[1] )%2 ;

    time_par[2] = ( date_buffer[25] + date_buffer[23] + date_buffer[22] + date_buffer[19] + date_buffer[18]
                    + date_buffer[17] + date_buffer[16] + date_buffer[15] + date_buffer[11] + date_buffer[10] + date_buffer[8]
                    + date_buffer[7] + date_buffer[6] + date_buffer[4] + date_buffer[2] )%2 ;

    time_par[3] = ( date_buffer[24] + date_buffer[21] + date_buffer[19] + date_buffer[18] + date_buffer[15]
                    + date_buffer[14] + date_buffer[13] + date_buffer[12] + date_buffer[11] + date_buffer[7] + date_buffer[6]
                    + date_buffer[4] + date_buffer[3] + date_buffer[2] + date_buffer[0] )%2 ;

    time_par[4] = ( date_buffer[25] + date_buffer[22] + date_buffer[20] + date_buffer[19] + date_buffer[16]
                    + date_buffer[15] + date_buffer[14] + date_buffer[13] + date_buffer[12] + date_buffer[8] + date_buffer[7]
                    + date_buffer[5] + date_buffer[4] + date_buffer[3] + date_buffer[1] )%2 ;
}

void computeDst()
{
    }

void createPacket()
{
    int sind = 12; // starting 12 to 0 bits of the sync time word
    int pind = 4; // 5 bits parity
    int tind = 24;
    int dstlsin= 4; 
    int dstnextind = 5;
    int reservedBit =0;
    int notice = 0;
    for (int i = 0 ; i <= 59; ++i) {
        if (i <= 12) {
            pm_packet[i] = sync_T[sind];
            --sind;
        } //done with sync_T
        else if (i <= 17 ) { // 13 to 17 is parity bits
            pm_packet [i] = time_par[pind];
            --pind;
        } else if ( i == 18) {
            pm_packet[i] = date_buffer[25];
        } else if ( i == 19) {
            pm_packet[i] = date_buffer[0];
        } else if ( i <= 28 ) {
            pm_packet[i] = date_buffer[tind];
            --tind;
        } else if ( i == 29) {
            pm_packet[i] = reservedBit;
        } else if ( i <= 38) {
            pm_packet[i] = date_buffer[tind];
            --tind;
        } else if ( i == 39) {
            pm_packet[i] = reservedBit;
        } else if ( i <= 46) {
            pm_packet[i] = date_buffer[tind];
            --tind;
        } else if (i <=48) {
            pm_packet[i] = dst_ls[dstlsin];
            --dstlsin;
        } else if ( i == 50) {
            pm_packet[i] = notice;
        } else if (i <= 52) {
            pm_packet[i] = dst_ls[dstlsin];
            --dstlsin;
        } else if (i <= 58) {
            pm_packet[i] = dst_ls[dstnextind];
            --dstnextind;
        } else {
            pm_packet[i] = 0;
        }
    }

}

void printPacket()
{
    for (int i =0; i <=59; ++i) {
        pc.printf("%d", pm_packet[i]);
    }
    pc.printf("\n\r");
    for (int i =0; i <=59; ++i) {
    pc.printf("%d", am_packet[i]);
    }
    pc.printf("\n\r");
    /*
    for (int i =0; i <=26; ++i) {
    pc.printf("%d", date_buffer[i]);
    }
    */
}

    

// This flag will indicate we are still in the current Tx if < 10
// Since ticket goes off every 100 ms after 10 x 100 = 1 sec - this is the rate 1bit/sec
int txSubCycle = 0;
int packetCycle =0;

// voltage level enums
#define VOLT_HIGH 0
#define VOLT_LOW 1
#define VOLT_LOW_N 2
#define VOLT_HIGH_N 3
#define VOLT_INVALID 100 // No voltage shall be output
#define INVALID_BIT 2
#define ONE_BIT 1

//voltage static variable
int currentVoltage = VOLT_INVALID; // by default at a 17 db low

void txBit(int index)
{
    unsigned char pm_bit = pm_packet[index];
    unsigned char am_bit = am_packet[index];
    
    
    //Transmit 0bit
    switch(txSubCycle) {

        case 0: { //do the 17 db drop in voltage
            currentVoltage = VOLT_LOW;
            voltage[packetCycle][0] = VOLT_LOW;
        }
        break;
        // we have done the 17 db low we need an immediate phase shift if needed - RX expects it right after 17 db hold for 100 msecs
        // We will hold volt low of phase change for 400 msec for both 0 and 1
        case 1: {
            if (pm_bit == 1) {
                currentVoltage = VOLT_LOW_N;
                voltage[packetCycle][1] = VOLT_LOW_N;
            } else {
                currentVoltage = VOLT_LOW;
                voltage[packetCycle][1] = VOLT_LOW;
            }
            
        }

        case 2: {
            if (am_bit == 0) {
                if (pm_bit == 0) {
                    currentVoltage = VOLT_HIGH;
                    voltage[packetCycle][2] = VOLT_HIGH;
                } else {
                    currentVoltage = VOLT_HIGH_N;
                    voltage[packetCycle][2] = VOLT_HIGH_N;
                }
            }
            break;
        }

        case 3: {
            break;
        }
        case 4: {
            break;
        }
        case 5: { // time to go high for 1
            if (am_bit == 1) {
                if (pm_bit == 1) {
                    currentVoltage = VOLT_HIGH_N;
                    voltage[packetCycle][5] = VOLT_HIGH_N;
                } else {
                    currentVoltage = VOLT_HIGH;
                   voltage[packetCycle][5] = VOLT_HIGH;
                }
            }
            break;
        }
        // Rest of the cycles keep holding the current voltage
        case 6:
        break;
        case 7:
        break;
        case 8: {
            if (am_bit == 2) {
                if (pm_bit == 1) {
                    currentVoltage = VOLT_HIGH_N;
                  voltage[packetCycle][8] = VOLT_HIGH_N;
                } else {
                    currentVoltage = VOLT_HIGH;
                    voltage[packetCycle][8] = VOLT_HIGH;
                }
            }
            break;
        }
        case 9:
            break;
        default :
            break;
    }

    if (txSubCycle == 9) {
        //reset we are done with 1 second
        txSubCycle =0;
         ++packetCycle;
         
         //voltage.push_back(subvector);
         //subvector.clear();
    } else if ( packetCycle == 59 )
    {
        packetCycle =0; //reset packet cycle
        
        //And compute the next timestamp to transmit
        //Get current minute count
      /* This portion is bieng commented out for testing
      * Should uncomment this for the real TX that transmits 
      * the next timestamp every minute 
      * 
        computeCurrentMinuteCounter();
        pc.printf("Current Time as minutes since January 1, 2000 = %d\n\r",minuteCounter);

        // compute the Tx Buffer
        createDateBuffer();
        
        //compute parity bits
        computeParityBits();
        
        //compute dst

        // Create Packet
        createPacket();
        */
        
        }
    else {
        //increment to next .1 sec cycle
        ++txSubCycle;
       
    }
}

static int tx_counter =0;
//Ticker interrupt handler
//TODO : Currently we are transmitting the syncT word over and over again for testing only.
//Once we have the tx_buffer ready in the correct format we can switch to that.

void tickerHandler()
{

    if (txSubCycle == 9) {
        // end of a 1 sec - tx current bit and increment counter to next bit
        
        if (tx_counter == (SIZE_PM_PACKET - 1)) {
            tx_counter = 0;
        } else {
            ++tx_counter;
        }
    } 
    
    txBit(tx_counter);
}

string printVoltage( int volt)
{
    switch(volt) {
        case VOLT_LOW:
            return "L";
            break;
        case VOLT_HIGH:
            return "H";
            break;
        case VOLT_HIGH_N:
            return "NH";
            break;
        case VOLT_LOW_N:
            return "NL";
            break;
        default:
        return "-";

    }
}
int main()
{

    // Input M and N
    unsigned short newM=12, newN=1;

    // Set system clock according to M, N.
    int clkspd = setSystemFrequency(0x3, 0x1, newM, newN);
    

    // Reconfigure the serial
    pc.baud(9600);
 //   pc.printf("CLOCK_SPEED %d\r\n", clkspd);

    /////////////////////////
    // Create Frequency
    /////////////////////////
    // Freq
    double freq = 60e3;
    // period
    double period = 1/freq;
    // resolution
    double res = period / (RESOLUTION); // Convert to milli second
  //  pc.printf("period = %.11f , res = %.11f, RESOLUTION = %d\r\n", period, res, RESOLUTION);

    /////////////////////////
    // Create VH/VL tables
    /////////////////////////
    generate_sin_table();
    
    set_time(1256729737);

    //Initialize minute counter to 1st jan 2000
    SetMinuteStartCount();
 //   pc.printf("1st January 2000 as minutes since January 1, 1970 = %d\n\r", minuteCountStart);
    //Get current minute count
    computeCurrentMinuteCounter();
 //   pc.printf("Current Time as minutes since January 1, 2000 = %d\n\r",minuteCounter);

    // compute the Tx Buffer
    createDateBuffer();

    // Create Packet
    createPacket();
    
    for (int i =0; i <60 ; ++i)
    {
     for (int j=0; j<10; ++j)
     voltage[i][j] = 9;
     }
     
    // setAm Buffers 
    setAmBuffer();
    //print packet
    printPacket();

    uint8_t index = 0;
    int count = 0;

    int pcnt = 0;
    const int maxdata = 1000;


    //ticker1.attach(&change2, 0.3);
    ticker1.attach(&tickerHandler, 0.1);

    //pc.printf("Start While loop - tx data\n\r");

    while (1) {
        
      // pc.printf("PC packet cycle %d\r\n", packetCycle);
    /*
        if (packetCycle == 10) {
            for (int i =0 ; i <=9 ; ++i) {
                pc.printf ("Packet #%d\n\r   ", i);
                for (int j=0; j < 10; ++j) {
                    pc.printf("%s ", printVoltage(voltage[i][j]));
                }
                pc.printf("\n\r");
            }
            exit(0);
        }
      */  
            //pc.printf (" ");

        switch(currentVoltage) {
            case VOLT_HIGH:
                aout.write_u16(VH[index&0x1f]);
                /*if (txSubCycle == 1) {
                    pc.printf("%d, ", VH[index&0x1f]);
                }*/
                break;
            case VOLT_LOW:
                aout.write_u16(VL[index&0x1f]);
                /*if (pcnt <maxdata) {
                    pc.printf("%d, ", VL[index&0x1f]);
                }*/
                break;
            case VOLT_LOW_N:
                aout.write_u16(NVL[index&0x1f]);
                /*if (pcnt <maxdata) {
                    pc.printf("%d, ", NVL[index&0x1f]);
                }*/
                break;
            case VOLT_HIGH_N:
                aout.write_u16(NVH[index&0x1f]);
                /*if (pcnt <maxdata) {
                    pc.printf("%d,", NVH[index&0x1f]);
                }*/
                break;
            default:
                //nothing is on analog out
                //  pc.printf("Invalid voltage %d\n\r", (int)currentVoltage);
                break;
        }
        pcnt++;
        index++;
        for (int i = 0; i < 8; i++) {
            for (count = 0; count < 10000000000000000000; count++) {};
        }
    }


}
