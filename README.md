# README #

### What is this repository for? ###

* This page is devoted to the creation of building an Atomic clock reciever from the NIST 60KHz transmission.

### How do I get set up? ###

# TRANSMITTER #
1. mBED LPC1768 (From WES Program)
2. Keil Debug Tools (Demo Version is fine), code is less then 32K bytes
3. Transmitter code is in the "tx_dma" directory as a Keil Project (uVision4, not FIVE)

# RECEIVER #

4. Receiver: LPC1769 Expresso Board (From Digikey, cost $40)
5. LPC Ware Tools (Free from LPC) an eclipse based debugger
6. The receiver is an Eclipse Project, under directory lpc_rx2

# LNA & TRANSMITTER #

7. LNA design work - LT Spice IV (free download)
8. Low Frequency Spectrum Analyzer (WES Program)
9. Frequency Generator (WES Program, or Analog Discovery Module)

# ANALOG PROTOTYPE #

1. Breadboard & Analog component kit 
2. We found the "Analog Discovery Kit" from Digilent very helpful
3. Student price $99 for the module. [http://www.digilentinc.com/](http://www.digilentinc.com/)
4. Add another $50 for the box of parts and a bread board
5. One team member also had real oscilloscope (very helpful)

# ANTENNAS #

1. Antennas made from Foam / Poster boards (20x30) for transmitter and receiver 
2. One spool of Magnet wire (transmitter 21 turns, receiver 5 turns)
3. Magnet wire (30awg) purchased from Fry's Electronics for about $5 per roll
4. Note: We assembled (soldered) a small circuit because the photo boards did not hold connections very well
5. We then hot-glued the TX amplifier circuit board to the foam board / antenna
6. We did the same for the RX LNA
7. The goal of the above was to make the hardware "more rigid" and not break if we moved wires

# POWER & CIRCUITS #

13. Transmitter Power Amplifier and LNA: Made from parts in the Discovery kit
14. Transmitter and receiver powered by 2 9v batteries (+/- 9V = 18v)
14. We soldered together a small proto board (Radio Shack) with the final design because the bread board did not have good solid connections, things came loose

# FUTURE CAPSTONE IDEAS #

1. Improve LNA and Receiver by creating a programmable gain amplifier, ours worked only in a perfect world :-(
2. Implement design on the Zed Board using only hardware, no software, with the idea of how small of can you make the design? Software API should be two 32bit registers (total 64bits) upper 4 bits could be used as 'status' ie: Signal is valid, and Data is ready
3. Implement design with other CPU type, ie: MSP430, specifically for ultra low power design reasons. 
4. Investigate ultra low power radio devices, for example: [http://dunkels.com/adam/dunkels11contikimac.pdf](http://dunkels.com/adam/dunkels11contikimac.pdf)

# Directories #

1. metadata - used by LPC Eclipse
2. rx_adc - early ADC code using software based sampling
3. rx_adc2 - DMA based ADC improved
4. rx_lpc - Switch to LPC tools
5. rx_lpc2 - final LPC based solution
6. tx_dac - Software based TX (non-DMA)
7. tx_dma - DMA based transmission using Keil Tools (Uvision 4)

# DETAILS - Theory #

1. THEORY_OF_OPERATION document in the REPO and the Wiki Download section
2. Link: [https://bitbucket.org/maswes/set_clock_wirelessly_atomic-cohort3/downloads/THEORY_OF_OPERATION_9jun2015.pdf](https://bitbucket.org/maswes/set_clock_wirelessly_atomic-cohort3/downloads/THEORY_OF_OPERATION_9jun2015.pdf)

### Who do I talk to? ###

* Team :  Eric Blum, Duane Ellis, Ramakrishna Thotakura, Tavares Forby, Neha Chahal