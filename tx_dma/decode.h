#include "mbed.h"
#include "MODDMA.h"
#include "stdint.h"
#include "string.h"
#include "math.h"

 
#if !defined(GLOBAL)
#define GLOBAL extern
#endif

/* how many LLIs do we have */
#define N_LLI        60
/* how long is a wave form */
#define WAVE_SAMPLES 16
/* We need 1.5 wave forms per table */
#define TOTAL_WAVE_SAMPLES (WAVE_SAMPLES + (WAVE_SAMPLES/2))
/* Our wave forms, low and high amplitude */
GLOBAL uint32_t wave_0_lo[TOTAL_WAVE_SAMPLES];
GLOBAL uint32_t wave_0_hi[TOTAL_WAVE_SAMPLES];
/* The Phase shifted form */
#define wave_1_lo  (&(wave_0_lo[ WAVE_SAMPLES / 2 ]))
#define wave_1_hi  (&(wave_0_hi[ WAVE_SAMPLES / 2 ]))


/* counts from 0 to 1000 */
GLOBAL int msec_counter;
/* time now in seconds */
GLOBAL uint32_t seconds_counter;
/* In the current second, what to transmit for AM */
GLOBAL int current_am_symbol;
/* In the current second, what to transmit for PHASE */
GLOBAL int current_ph_symbol;
GLOBAL int dbg_print_packet_flag;
void dbg_print_packet(void);

void determine_transmit_symbol(void);

extern Serial pc;

/* For TX packet data */
void init_real_time();
void setup_packets();
