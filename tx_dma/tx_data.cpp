#include "decode.h"

int DUANE_TESTING = 0;
int INCREMENT_MINUTE = 0; // 0 does not increment the minute, 1 increments the minute

#define N_TEST_POINTS 4
#define N_REAL_POINTS 60

int test_point_num = 0;
int real_point_num = 0;

// PM data packet
#define SIZE_DATE_BUFFER 26 // 26  bit date word
#define SIZE_SYNCT_BUFFER 13 //synchronization word length
#define SIZE_PACKET 60
static unsigned char real_phase[SIZE_PACKET] = {9}; // 9 is invalid
static unsigned char real_am[SIZE_PACKET] = {9}; // 9 is invalid
static unsigned char date_buffer[SIZE_DATE_BUFFER] = {0};
static unsigned char sync_T[SIZE_SYNCT_BUFFER] = {0,0,0,1,0,1,1,0,1,1,1,0,0}; //Time synchronization word
static unsigned char dst_ls[5] = {0};
static unsigned char dst_next[5] = {0};
static unsigned char time_par[5] = {0};
static int minuteCounter = 0;

// AM data buffer
unsigned char min_buffer[7];
unsigned char year_buffer[8];
unsigned char day_buffer[10];
unsigned char hour_buffer[6];

static int AM_minute =0;
static int AM_day =0;
static int AM_year =0;
static int AM_hour =0;

static int test_phase[N_TEST_POINTS] = {	
	0,0,0,1 };

static int test_am[] = {
	1,1,1,2 };
	


void real_determine_transmit_symbol(void)
{
	/* INSERT THE REAL CODE HERE */
	/* YOUR JOB:
	 *   Based on "seconds_counter"
	 *   Set:  current_am_symbol
	 *   Set:   current_ph_symbol
	 */
	current_am_symbol = real_am[ real_point_num ];
	current_ph_symbol = real_phase[ real_point_num ];
	
	//pc.printf("Transmitting: AM %d : PM %d\r\n", real_am[real_point_num],real_phase[real_point_num]);

	
	real_point_num += 1;
	if( real_point_num >= N_REAL_POINTS ){
		// Increment time
		if (INCREMENT_MINUTE) 
		{
			minuteCounter++;
			if (AM_hour == 23 && AM_minute == 59) {
				AM_hour = 0;
				AM_minute = 0;
			} else if (AM_minute == 59) {
				AM_minute = 0;
				AM_hour++;
			} else {
				AM_minute++;
			}
		}
		
		setup_packets();
		real_point_num = 0;
	}

}

void duane_determine_transmit_symbol( void )
{
	current_am_symbol = test_am[ test_point_num ];
	current_ph_symbol = test_phase[ test_point_num ];
	test_point_num += 1;
	if( test_point_num >= N_TEST_POINTS ){
		test_point_num = 0;
	}
}




void determine_transmit_symbol( void )
{
	if( DUANE_TESTING ){
		duane_determine_transmit_symbol();
	} else {
		real_determine_transmit_symbol();
	}
}

void init_real_time()
{
	  // seting unix time
	  set_time(1256729737);
	
		// get the time in seconds for 1st January 2000
    // setup time structure for Wed, 28 Oct 2009 11:35:37
    struct tm t;
    t.tm_sec = 0;    // 0-59
    t.tm_min = 0;    // 0-59
    t.tm_hour = 0;   // 0-23
    t.tm_mday = 1;   // 1-31
    t.tm_mon = 0;     // 0-11 months since Jan
    t.tm_year = 100;  // year since 1900 +100 = 2000

    // convert to timestamp and display (1256729737)
		// seconds = Jan 1 2000 - Jan 1 1970
    time_t seconds = mktime(&t);
    
	  //convert seconds to minutes for our timestamp calculation
    int minuteCountStart = seconds/60;
	
		// current time now from Jan 1 1970 until now
		time_t currentTime;
    time(&currentTime); // time in seconds
    
		//convert to minutes since January 1, 1970
    //currentTime = currentTime/60;
    //compute the current time since 1st jan 2000 as a minute counter.
    minuteCounter = (currentTime/60) - minuteCountStart;
		
		// Need to get AM time here
		struct tm *info;
		info = localtime( &currentTime );
		pc.printf("Local time %s\r\n", asctime(info));
		
		AM_minute = info->tm_min;
		AM_day = info->tm_yday;
		AM_year = info->tm_year - 100; // tm_year is since 1900.  we want from 2000
		AM_hour = info->tm_hour;
		
		setup_packets();
}

bool getBit(int i)
{
    int mask = 1;
    mask = mask<<i;

    if ( (minuteCounter & mask ) == 0)
        return 0;
    else
        return 1;
}

void createPhaseDateBuffer()
{
    // translate minuteCounter into 26 bit time word
    for (int i =0; i < 26; i++) {
        date_buffer[i]= (int)getBit(i);
    }
}

/*
time_par[0] = sum(modulo 2){time[23, 21, 20, 17, 16, 15, 14, 13, 9, 8, 6, 5, 4, 2, 0]}
time_par[1] = sum(modulo 2){time[24, 22, 21, 18, 17, 16, 15, 14, 10, 9, 7, 6, 5, 3, 1]}
time_par[2] = sum(modulo 2){time[25, 23, 22, 19, 18, 17, 16, 15, 11, 10, 8, 7, 6, 4, 2]}
time_par[3] = sum(modulo 2){time[24, 21, 19, 18, 15, 14, 13, 12, 11, 7, 6, 4, 3, 2, 0]}
time_par[4] = sum(modulo 2){time[25, 22, 20, 19, 16, 15, 14, 13, 12, 8, 7, 5, 4, 3, 1]}
*/
void computeParityBits()
{
    time_par[0] = ( date_buffer[23] + date_buffer[21] + date_buffer[20] + date_buffer[17] + date_buffer[16]
                    + date_buffer[15] + date_buffer[14] + date_buffer[13] + date_buffer[9] + date_buffer[8] + date_buffer[6]
                    + date_buffer[5] + date_buffer[4] + date_buffer[2] + date_buffer[0] )%2;

    time_par[1] = ( date_buffer[24] + date_buffer[22] + date_buffer[21] + date_buffer[18] + date_buffer[17]
                    + date_buffer[16] + date_buffer[15] + date_buffer[14] + date_buffer[10] + date_buffer[9] + date_buffer[7]
                    + date_buffer[6] + date_buffer[5] + date_buffer[3] + date_buffer[1] )%2 ;

    time_par[2] = ( date_buffer[25] + date_buffer[23] + date_buffer[22] + date_buffer[19] + date_buffer[18]
                    + date_buffer[17] + date_buffer[16] + date_buffer[15] + date_buffer[11] + date_buffer[10] + date_buffer[8]
                    + date_buffer[7] + date_buffer[6] + date_buffer[4] + date_buffer[2] )%2 ;

    time_par[3] = ( date_buffer[24] + date_buffer[21] + date_buffer[19] + date_buffer[18] + date_buffer[15]
                    + date_buffer[14] + date_buffer[13] + date_buffer[12] + date_buffer[11] + date_buffer[7] + date_buffer[6]
                    + date_buffer[4] + date_buffer[3] + date_buffer[2] + date_buffer[0] )%2 ;

    time_par[4] = ( date_buffer[25] + date_buffer[22] + date_buffer[20] + date_buffer[19] + date_buffer[16]
                    + date_buffer[15] + date_buffer[14] + date_buffer[13] + date_buffer[12] + date_buffer[8] + date_buffer[7]
                    + date_buffer[5] + date_buffer[4] + date_buffer[3] + date_buffer[1] )%2 ;
}

void createPhasePacket()
{
		
    int sind = 12; // starting 12 to 0 bits of the sync time word
    int pind = 4; // 5 bits parity
    int tind = 24;
    int dstlsin= 4; 
    int dstnextind = 5;
    int reservedBit =0;
    int notice = 0;
    for (int i = 0 ; i <= 59; ++i) {
        if (i <= 12) {
            real_phase[i] = sync_T[sind];
            --sind;
        } //done with sync_T
        else if (i <= 17 ) { // 13 to 17 is parity bits
            real_phase [i] = time_par[pind];
            --pind;
        } else if ( i == 18) {
            real_phase[i] = date_buffer[25];
        } else if ( i == 19) {
            real_phase[i] = date_buffer[0];
        } else if ( i <= 28 ) {
            real_phase[i] = date_buffer[tind];
            --tind;
        } else if ( i == 29) {
            real_phase[i] = reservedBit;
        } else if ( i <= 38) {
            real_phase[i] = date_buffer[tind];
            --tind;
        } else if ( i == 39) {
            real_phase[i] = reservedBit;
        } else if ( i <= 46) {
            real_phase[i] = date_buffer[tind];
            --tind;
        } else if (i <=48) {
            real_phase[i] = dst_ls[dstlsin];
            --dstlsin;
        } else if ( i == 50) {
            real_phase[i] = notice;
        } else if (i <= 52) {
            real_phase[i] = dst_ls[dstlsin];
            --dstlsin;
        } else if (i <= 58) {
            real_phase[i] = dst_ls[dstnextind];
            --dstnextind;
        } else {
            real_phase[i] = 0;
        }
    }

}

void createAMPacket()
{
		// min, hour, day, year place
		// unsigned char min_buffer[7];
		// unsigned char hour_buffer[6];
    // unsigned char day_buffer[10];
    // unsigned char year_buffer[8];
		
		// minute
		real_am[1] = min_buffer[6];
		real_am[2] = min_buffer[5];
		real_am[3] = min_buffer[4];
		real_am[5] = min_buffer[3];
		real_am[6] = min_buffer[2];
		real_am[7] = min_buffer[1];
		real_am[8] = min_buffer[0];
		
		// hour
		real_am[12] = hour_buffer[5];
		real_am[13] = hour_buffer[4];
		real_am[15] = hour_buffer[3];
		real_am[16] = hour_buffer[2];
		real_am[17] = hour_buffer[1];
		real_am[18] = hour_buffer[0];
		
		// day
		real_am[22] = day_buffer[9];
		real_am[23] = day_buffer[8];
		real_am[25] = day_buffer[7];
		real_am[26] = day_buffer[6];
		real_am[27] = day_buffer[5];
		real_am[28] = day_buffer[4];
		real_am[30] = day_buffer[3];
		real_am[31] = day_buffer[2];
		real_am[32] = day_buffer[1];
		real_am[33] = day_buffer[0];
		
		// year
		real_am[45] = year_buffer[7];
		real_am[46] = year_buffer[6];
		real_am[47] = year_buffer[5];
		real_am[48] = year_buffer[4];
		real_am[50] = year_buffer[3];
		real_am[51] = year_buffer[2];
		real_am[52] = year_buffer[1];
		real_am[53] = year_buffer[0];
		
		// Marker place
    real_am[0] = 2; 
    real_am[9] = 2;
    real_am[19] = 2;
    real_am[29] = 2;
    real_am[39] = 2;
    real_am[49] = 2;
    real_am[59] = 2;
		
		// all zero place
		real_am[4] = 0;
		real_am[10] = 0;
		real_am[11] = 0;
		real_am[14] = 0;
		real_am[20] = 0;
		real_am[21] = 0;
		real_am[24] = 0;
		real_am[34] = 0;
		real_am[35] = 0;
		real_am[44] = 0;
		real_am[54] = 0;
		
		// Misc bit set
		// UT
		real_am[36] = 0; 
		real_am[37] = 0; 
		real_am[38] = 0; 
		real_am[40] = 0; 
		real_am[41] = 0; 
		real_am[42] = 0; 
		real_am[43] = 0; 

		// LYI, LSW, DST[]
		real_am[55] = 0; 
		real_am[56] = 0; 
		real_am[57] = 0; 
		real_am[58] = 0; 

}

void mod_function(int number, unsigned char* buffer)
{
    int div[10]= {200, 100, 80, 40, 20, 10, 8, 4, 2, 1};
    int remain = 0;

    int buffer_ind = 9;

    for (int i = 0; i < 10; ++i)
    {
        remain = number / div[i];
        if (remain)
        {
            buffer[buffer_ind] = 1;
            number = number - div[i];
        }
        --buffer_ind;
    }
}

void createAMDateBuffer ()
{	
	  mod_function(AM_year, year_buffer);
    mod_function(AM_hour, hour_buffer);
    mod_function(AM_day, day_buffer);
    mod_function(AM_minute, min_buffer);
}

void checkPacket()
{
	for (int i = 0; i < 60; i++)
	{
		if (real_am[i] == 9 || real_phase[i] == 9)
		{
			pc.printf("Incorrect values for either real_am[%d] = %d or real_phase[%d] = %d\r\n", i, real_am[i], i, real_phase[i]);
			exit(0);
		}
	}
}

void setup_packets()
{
	/* Create Phase Packet */
	// create 26 bit date buffer
	createPhaseDateBuffer();
	
	// computer 5 parity bits
	computeParityBits();
	
	// create phase packet
	createPhasePacket();
	
	/* Create AM Packet */
	// Create AM data buffer
	createAMDateBuffer();
	
	// Create AM packet
	createAMPacket();
	
	// Check packet sanity
	checkPacket();
	
	// printing packets
	dbg_print_packet_flag = 1;
}

void dbg_print_packet(void)
{
	pc.printf(" Bit Numb: ");
	int i;
	for( i = 0 ; i < SIZE_PACKET ; i++ ){
		switch(i%10){
			case 0:
				pc.printf("%-2d", i/10);
				break;
			case 1:
				/* skip */
				break;
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
				pc.printf(" ");
				break;
			case 9:
				pc.printf("  ");
				break;
		}
	}
	pc.printf("\r\n");
	pc.printf("AM PACKET: ");
	for (int i = 0; i < SIZE_PACKET; i++) { 
		pc.printf("%d", real_am[i]);
		if( (i%10)==9 ){
			pc.printf(" ");
		}
	}
	pc.printf("\r\n");

	pc.printf("PM PACKET: ");
	for (int i = 0; i < SIZE_PACKET; i++) {
		pc.printf("%d", real_phase[i]);
		if( (i%10)==9 ){
			pc.printf(" ");
		}
	}
	pc.printf("\r\n\r\n");
}

