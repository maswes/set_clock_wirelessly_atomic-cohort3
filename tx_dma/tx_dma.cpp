#define GLOBAL
#include "decode.h"

#define DMA_DEBUG   1
#define CLOCK_DEBUG 1

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);
Serial pc(USBTX, USBRX);

/* private to the transmitter */
 int old_ph_symbol;

/*
** Assumption:
**   96mhz - main clock
**   DMA Divisor = 100
** Gives:
**   960khz which is less then 1mhz
** Data Table: 16 samples
**   960khz / 16 = 60khz
**/

MODDMA dma;
MODDMA_LLI lliA[N_LLI];
MODDMA_LLI lliB[N_LLI];
MODDMA_Config dmacfg;
int current_lli_letter;


// Function prototypes for IRQ callbacks.
// See definitions following main() below.
void TC0_callback(void);
void ERR0_callback(void);

#define DAC_CHNL_NUM  MODDMA::Channel_0
#define DAC_DATA_REG ((uint32_t)(&(LPC_DAC->DACR)))


static void setup_lli( int phasevalue, int mag_value )
{
	int x;
	MODDMA_LLI *pLLI;
	uint32_t srcvalue;

//  Debugging.. force values.
//	phasevalue = 0;
//	mag_value = 'H';
	
	/* Which LLI group are we going to use? */
	if( current_lli_letter == 'A' ){
		pLLI = &lliA[0];
	} else {
		pLLI = &lliB[0];
	}

	/* What phase, and which magnitude */
	/* this chooses the wave form */
	if( phasevalue == 0 ){
		if( mag_value == 'L' ){
			srcvalue = ((uint32_t)(wave_0_lo));
		} else {
			srcvalue = ((uint32_t)(wave_0_hi));
		}
	} else {
		if( mag_value == 'L' ){
			srcvalue = ((uint32_t)(wave_1_lo));
		} else {
			srcvalue = ((uint32_t)(wave_1_hi));
		}
	}
	/* now - plug in the srce addresses to all LLIs */
	for( x = 0 ; x < N_LLI ; x++ ){
		pLLI[x].srcAddr( srcvalue );
	}
}

void setup_dma( void	)
{
	uint32_t v;
	int x;
    MODDMA_Config *cfg;
// Setup active DMA configiuration
// we start by setting it Buffer A
    cfg = &(dmacfg);
    cfg->channelNum( DAC_CHNL_NUM )
    ->srcMemAddr    ( (uint32_t)(&wave_0_lo[0]) )
		->dstMemAddr    ( MODDMA::DAC )
    ->transferSize  ( WAVE_SAMPLES )
    ->transferType  ( MODDMA::m2p )
    ->dstConn       ( MODDMA::DAC )
    // When done, we goto lliA 1 (next A)
    ->dmaLLI        ( (uint32_t) (&lliA[1]))
    ->attach_tc     ( &TC0_callback )
    ->attach_err    ( &ERR0_callback )
    ; // end conf.

	// we start with A ... so set A as the letter 
		current_lli_letter = 'A';
    // Prepare configuration.
    dma.Setup( cfg );
		
    LPC_GPDMACH_TypeDef *pChannel = (LPC_GPDMACH_TypeDef *)dma.Channel_p( cfg->channelNum() );

		/* MODDMA - Enables the IRQ 
		 * We do not want this
		 */
		 
		 
		 
		 /* Setup our bank of LLIs */
		 for( x = 0 ; x < N_LLI ; x++ ){
			 /* Set the destination address */
			 lliA[x].dstAddr( DAC_DATA_REG );
			 lliB[x].dstAddr( DAC_DATA_REG );
			 /* put a zero here for now */
			 /* will be setup by setup_lli() */
			 lliA[x].srcAddr( 0 );
			 lliB[x].srcAddr( 0 );
			 /* use the same as the DMAC */
			 v = pChannel->DMACCControl;
			 v = v | WAVE_SAMPLES;
			 v = v &(~(1U<<31));
			 lliA[x].control( v );
			 lliB[x].control( v );
			 
			 /* Each "A" points to the next A... */
			 lliA[x].nextLLI( ((uint32_t)(&lliA[x+1])) );
			 /* Each "B" points to the next B... */
			 lliB[x].nextLLI( ((uint32_t)(&lliB[x+1])) );			 
		 }
		 
		 /* Make the last A point to the first B */
		 /* And the last B point to the first A */
		 lliA[ N_LLI-1 ].nextLLI( ((uint32_t)(&(lliB[0]))));
		 lliB[ N_LLI-1 ].nextLLI( ((uint32_t)(&(lliA[0]))));
		 
		 /* We also want the *LAST* element to have the IRQ enabled */
		 v = pChannel->DMACCControl;
		 v = v | WAVE_SAMPLES;
		 v = v | (1U<<31);
		 lliA[ N_LLI-1 ].control(v);
		 lliB[ N_LLI-1 ].control(v);
		 
		 /* configure the LLI with data */
		current_lli_letter = 'A';
		setup_lli( 0, 'L' );
		current_lli_letter = 'B';
		setup_lli( 0, 'L' );

		 /* we start with "A" so set it to A */
 		current_lli_letter = 'A';

}

void setup_clocks(void)
{   
	uint32_t v;

	/* DAC gets a 96mhz clock */
  v = LPC_SC->PCLKSEL0; 
  v &= ~(0x3 << 22);
	/* Select PCLK / 1 */
  v |= (0x1 << 22);
  LPC_SC->PCLKSEL0 = v;
  
	/* Enable the DAC in pin select */
	v = LPC_PINCON->PINSEL1;
	v = v & (~(3<<20));
	v = v | (2 << 20);
	LPC_PINCON->PINSEL1 = v;
	
	/* The DAC counts 100 clocks
	 * then requests data
	 * Given 96mhz / 100 = 960khz dac rate
	 * Given 16 points per wave 960/16 = 60khz
	 * Gives us our 60khz wave form
	 *
	 * The value programed is "n-1"
	 */
	LPC_DAC->DACCNTVAL = 100-1;
}



void build_waveform(void)
{
	int x;
	int frac;
	double radians;
	double sinval;
	uint32_t v;
	
	for( x = 0 ; x < TOTAL_WAVE_SAMPLES ; x++ ){
		frac = x % WAVE_SAMPLES;
		radians = 2 * 3.14159 * frac;
		radians = radians / ((double)(WAVE_SAMPLES));
		
		/* we do not use full 32767 as full scale
		 * we back off so we do not 'rail' the input to
		 * the ADC, why? Beause sometimes I get zeros
		 * and other times i get full scale
		 *
		 * I cannot prove the data I receive is correct
		 * or if it is incorrect, by using less then 
		 * the full 32768 scale .. we never get all zeros
		 * and we never get all 1s in our ADC result
		 *
		 * 31000 = +0.3 to 3.0 volt swing
		 */
#define FULLSCALE (31000.0)
		sinval = 32768.0 + (FULLSCALE * sin(radians) / 7.0);
		v = ((uint32_t)(sinval)) & 0x0ffc0;
		/* We want the data to be fast so set the BIAS bit */
		v = v | (0 << 16);
		wave_0_lo[x] = v;
		
		sinval = 32768.0 + (FULLSCALE * sin(radians));
		v = ((uint32_t)(sinval)) & 0x0ffc0;
		/* Set the BIAS bit */
		v = v | (0 << 16);
		wave_0_hi[x] = v;
	}
}

int main()
{
	// Setup the serial port to print out results.
  pc.baud(9600);
  pc.printf("DMA with DMA example\r\n");
	pc.printf("BUILD DATE: %s TIME: %s\r\n", __DATE__, __TIME__ );
  pc.printf("===================\r\n");
  // We use the ADC irq to trigger DMA and the manual says
  // that in this case the NVIC for ADC must be disabled.

	build_waveform();
	
	// createst the inital time packet
	init_real_time();
	
  setup_clocks();

	setup_dma();
	
  dma.Enable( &(dmacfg) );
	/* Turn on the DMA
	**  Bit0 is a status bit, we write a 0
	**  DBLBUF_ENA (bit 1)
	**  CNT_ENA    (bit 2)
	**  DMA_ENA    (bit 3)
	*/
	LPC_DAC->DACCTRL = (1 << 3) | (1<<2) | (0<<1) | (0<<0);
	
  
	/* The main loop has nothing to do */
  while (1) {
		
			__nop();
		if( dbg_print_packet_flag ){
			dbg_print_packet_flag = 0;
			dbg_print_packet();
		}
	}
}


/* this is called every 1 millisecond */
static void update_wave_form_table(void)
{
	int am;
	int ph;
	
	/* Determine the phase: 0 or 1 */
	if( msec_counter < 100 ){
		/* phase changes at 100 mSecs */
		/* until then use the old phase symbol */
		ph = old_ph_symbol;
	} else {
		/* remember this for the next second */
		ph = current_ph_symbol;
		old_ph_symbol = ph;
	}
	
	/* Determine the AM L or H */
	switch( current_am_symbol ){
	case 0:
		if( msec_counter < 200 ){
				am = 'L';
		} else {
				am = 'H';
		}
		break;
	case 1:
		if( msec_counter < 500 ){
			am = 'L';
		} else {
			am = 'H';
		}
		break;
	case 2: /* marker */
		if( msec_counter < 800 ){
			am = 'L';
		} else {
			am = 'H';
		}
		break;
	}
	led1 = (am=='H') ? 1 : 0;
	led2 = ph;
	if( msec_counter > 500 ){
		led3 = 1;
	} else {
		led3 = 0;
	}
	setup_lli( ph, am );
}

// Configuration callback on TC
void TC0_callback(void)
{
	/* this interrupt occurs at the end of 
	 * N_LLI wave forms are complete
	 */
  int x;

#if N_LLI != 60
	#error This code assumes a 1 millisecond count rate
#endif
	//led1 = x & 1;	
	x = msec_counter;
	x = x + 1;
	
	if( x == 1000 ){
		x = 0;
		seconds_counter += 1;
		determine_transmit_symbol();
	}
	msec_counter = x;
	current_lli_letter ^= 0x03;
	update_wave_form_table( );
}

// Configuration callback on Error
void ERR0_callback(void)
{
    // Switch off burst conversions.
    LPC_DAC->DACCTRL = 0;
    dma.clearErrIrq();
    error("Oh no! My Mbed EXPLODED! :( Only kidding, go find the problem");
}
