#include "mbed.h"
#include "MODDMA.h"

/* Goal 200khz adc sample rate.
 *   1 millisecond = 200 samples
 * To reduce the IRQ overhead we use 400
 * Thus, we get an IRQ every 2 milliseconds
 */
#define BUFFER_LENGTH 400 // length is in samples
#define XTAL_FREQ       12000000

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);
Serial pc(USBTX, USBRX);
int totalComplete = 0;

MODDMA dma;
MODDMA_LLI lliA;
MODDMA_LLI lliB;
MODDMA_Config dmacfg;
uint32_t bufA[BUFFER_LENGTH];
uint32_t bufB[BUFFER_LENGTH];

#define ADC_DATA_REGISTER ((uint32_t)(&LPC_ADC->ADGDR))

// Function prototypes for IRQ callbacks.
// See definitions following main() below.
void TC0_callback(void);
void ERR0_callback(void);

void setup_dma( void )
{
    MODDMA_Config *cfg;
// Setup active DMA configiuration
// we start by setting it Buffer A
    cfg = &(dmacfg);
    cfg->channelNum( MODDMA::Channel_0 )
    ->srcMemAddr    ( ADC_DATA_REGISTER )
    ->dstMemAddr    ( (uint32_t)(&bufA[0]))
    ->transferSize  ( BUFFER_LENGTH )
    ->transferType  ( MODDMA::p2m )
    ->transferWidth ( MODDMA::word ) //ARM : Word size is 32-bits
    ->srcConn       ( MODDMA::ADC )
    ->dstConn       ( 0 )
    // When done, we goto lliB
    ->dmaLLI        ( (uint32_t) (&lliB))
    ->attach_tc     ( &TC0_callback )
    ->attach_err    ( &ERR0_callback )
    ; // end conf.

    // Prepare configuration.
    dma.Setup( cfg );

    LPC_GPDMACH_TypeDef *pChannel = (LPC_GPDMACH_TypeDef *)dma.Channel_p( cfg->channelNum() );
    //lliA.srcAddr( pChannel->DMACCSrcAddr );
    //lliB.srcAddr( pChannel->DMACCSrcAddr );

    lliA.srcAddr(0);
    lliB.srcAddr(0);

    lliA.dstAddr( (uint32_t)(&bufA[0]) );
    lliB.dstAddr( (uint32_t)(&bufB[0]) );

    lliA.nextLLI( (uint32_t)(&lliB) );
    lliB.nextLLI( (uint32_t)(&lliA) );

    //lliA.control( pChannel->DMACCControl + BUFFER_LENGTH );
    //lliB.control( pChannel->DMACCControl + BUFFER_LENGTH );

    lliA.Control = dma.CxControl_TransferSize((uint32_t)BUFFER_LENGTH) 
                | dma.CxControl_SBSize(1UL) 
                | dma.CxControl_DBSize(1UL) 
                | dma.CxControl_SWidth(32UL) 
                | dma.CxControl_DWidth(32UL) 
                | dma.CxControl_DI() 
                | dma.CxControl_I();

    lliB.Control = dma.CxControl_TransferSize((uint32_t)BUFFER_LENGTH) 
                | dma.CxControl_SBSize(1UL) 
                | dma.CxControl_DBSize(1UL) 
                | dma.CxControl_SWidth(32UL) 
                | dma.CxControl_DWidth(32UL) 
                | dma.CxControl_DI() 
                | dma.CxControl_I();

}

void setup_clocks(void)
{
    uint32_t v;
    int m,n,cclkdiv,Fcco,cclk;
    /* We don't have to do much
     * The boot code sets it to 96mhz
     */
    m = (LPC_SC->PLL0CFG & 0xFFFF) + 1;
    n = (LPC_SC->PLL0CFG >> 16) + 1;
    cclkdiv = LPC_SC->CCLKCFG + 1;
    Fcco = (2 * m * XTAL_FREQ) / n;
    cclk = Fcco / cclkdiv;

    /* Configure Peripheral Clock0 (ADC)
     * To operate at DIV_1 = 96mhz
     */
    v = LPC_SC->PCLKSEL0;
    v &= ~(0x3 << 24);
    v |= (0x1 << 24);
    LPC_SC->PCLKSEL0 = v;

    pc.printf("PLL0CFG: 0x%08x\n", LPC_SC->PLL0CFG );
    pc.printf("CCLKCFG: 0x%08x\n",LPC_SC->CCLKCFG );
    pc.printf("M=%d, N=%d, XTAL_FREQ=%d\n", m, n, XTAL_FREQ );
    pc.printf("Fcco = %d\n", Fcco);
    pc.printf("cclk = %d\n", cclk);

    /* The clock into the Peripheral Block = 96mhz */
    pc.printf("PCLKSEL0 = 0x%08x\n", LPC_SC->PCLKSEL0 );

}

void setup_adc(void)
{
    uint32_t v;
    LPC_PINCON->PINSEL3 &= ~(3UL << 30);  /* P1.31, Mbed p20. */
    LPC_PINCON->PINSEL3 |=  (3UL << 30);


    /* Power up the ADC */
    LPC_SC->PCONP    |=  (1UL << 12);

    /* The PCLOCK = 96mhz
     * The input to the ADC must be less then 13mhz
     * So we divide by 8 - giving 12mhz
     */

    /* build the ADC Control Register */
    v = 0;
    /* select channel 5 bits[7:0]*/
    v |= (1 << 5);
    /* Clock Divisor, bits[15:8] */
    /*  GIVEN: 96mhz peripheral clock
     * DIVIDE:  8
     * RESULT: 12mhz ADC clock
     */
    int adc_clock_div = 8;
    v |= ((adc_clock_div - 1 ) << 8 );
    /* Burst Mode bit[16]*/
    v |= (1 << 16);
    /* reserved zero, bits[20:17] */
    v |= 0;

    /* Enable the ADC bit[21] */
    v |= (1UL << 21);
    /* bits[23:22] = reserved, zero */
    v |= 0;
    /* Bits[26:24] = No start setting */
    v |= 0;
    /* MUST BE zero = See data sheet and below */

    /* bits[27] = not applicable */
    v |= 0;
    /* bits[31:28] = reserved zero */
    v |= 0;
    LPC_ADC->ADCR =  v;

    /* interrrupt enable: ADGINTEN = 0 */
    /* From the data sheet:
     * Remark: START bits must be 000 when BURST = 1
     * or conversions will not start. If BURST is set to 1,
     * the ADGINTEN bit in the AD0INTEN register
     * (Table 534) must be set to 0.
     */
    //LPC_ADC->ADINTEN = 0;
    LPC_ADC->ADINTEN = 0x100;
}

void print_reg( const char *name, uint32_t a )
{
    uint32_t v;
    uint32_t *p;

    p = ((uint32_t *)(a));

    v = *p;

    pc.printf("%-15s: 0x%08x: 0x%08x\n", name, a, v );
}

void print_lli( uint32_t v )
{
    pc.printf( "LLI at 0x%08x\n", v );
    print_reg( "lli0.src"    ,v+0x00 );
    print_reg( "lli0.dst"    ,v+0x04 );
    print_reg( "lli0.lli"    ,v+0x08 );
    print_reg( "lli0.ctrl"   ,v+0x0c );
}

void print_dma_config(void)
{
    uint32_t v;
    pc.printf("Variable addresses\n");
    pc.printf("BufA = 0x%08x\n", ((uint32_t)(&bufA[0])));
    pc.printf("BufB = 0x%08x\n", ((uint32_t)(&bufB[0])));
    pc.printf("lliA = 0x%08x\n", ((uint32_t)(&lliA)) );
    pc.printf("lliB = 0x%08x\n", ((uint32_t)(&lliB)) );

    pc.printf("Registers\n");
    print_reg("DMACC0SrcAddr" , 0x50004100 );
    print_reg("DMACC0DestAddr", 0x50004104 );
    print_reg("DMACC0LLI"     , 0x50004108 );
    print_reg("DMACC0Control" , 0x5000410C );
    print_reg("DMACC0Config"  , 0x50004110 );

    /* Fetch the LLI from the DMA controller channel 0 */
    v = *((uint32_t *)(0x50004108));
    /* and print the lli structure from memory */
    print_lli( v );

    /* Fetch the next LLI pointer */
    v = v + 8;
    v = *((uint32_t *)(v));
    /* and print it */
    print_lli( v );
}




int main()
{
    // Setup the serial port to print out results.
    pc.baud(115200);
    pc.printf("ADC with DMA example\n");
    pc.printf("====================\n");

    // We use the ADC irq to trigger DMA and the manual says
    // that in this case the NVIC for ADC must be disabled.
    NVIC_DisableIRQ(ADC_IRQn);

    setup_clocks();

    setup_dma();

    setup_adc();

    print_dma_config();

    // Enable configuration.
    dma.Enable( &(dmacfg) );

    while (1) {
        if(totalComplete> 5000) {
            break;
        }

        wait(0.5);
        pc.printf("Complete: %d\n", totalComplete);
    }

    for(int i = 0; i < BUFFER_LENGTH; i++) {
        int iVal = (bufA[i] >> 4) & 0xFFF;
        printf("%d %d\n", i, iVal );
    }

    for (int i = 0; i < BUFFER_LENGTH; i++) {
        int iVal = (bufB[i] >> 4) & 0xFFF;
        printf("%d %d\n", i + BUFFER_LENGTH, iVal );
    }

    pc.printf("Counter : %d \n",totalComplete); // Counter
    pc.printf("Program exit.");


}




// Configuration callback on TC
void TC0_callback(void)
{
    int x;
    x = totalComplete;
    x = x + 1;
    totalComplete = x;
    // Switch on LEDs to show transfer complete.
    // Make the LEDs count in Binary the number of transfer completes.
    x = x >> 16;
    led1 = (!!(x & 1));
    led2 = (!!(x & 2));
    led3 = (!!(x & 4));
    led4 = (!!(x & 8));
}

// Configuration callback on Error
void ERR0_callback(void)
{
    // Switch off burst conversions.
    LPC_ADC->ADCR |= ~(1UL << 16);
    LPC_ADC->ADINTEN = 0;
    dma.clearErrIrq();
    error("Oh no! My Mbed EXPLODED! :( Only kidding, go find the problem");
}